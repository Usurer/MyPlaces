## My Places

The purpose of this project is to create an application 
that would allow creation and management of interesting 
places and hiking routes on a Google Map. Vanilla JS on frontend and ASP.NET on backend.

Expected features are:
1. Add a Point, Line, or a Polygon to the map.
2. Provide description (text, photo) to points, lines and polygons.
3. Edit created items.
4. Import data from KML files.
5. Use Google OAuth to differentiate content between users.
6. Get an access to created items by an unique link - i.e. only this item should be visible on a map.


Current state of the application is an MVC with a bunch of bugs.