'use strict';

const gulp = require('gulp');
const replace = require('gulp-replace');
const zip = require('gulp-zip');

function prepareViewerUIPackage() {
    return gulp.src(['./Viewer/**', '!./Viewer/node_modules/**'] )
        .pipe(replace(/http:\/\/localhost:\d+/g, 'http://18.216.128.128:8084'))
        .pipe(zip('ViewerUI_Package.zip'))
        .pipe(gulp.dest('./Packages'));
}

function prepareEditorUIPackage() {
    return gulp.src(['./PoC/**', '!./PoC/node_modules/**', '!./PoC/.vscode/**', '!./PoC/MDB/**'] )
        .pipe(replace(/http:\/\/localhost:\d+/g, 'http://18.216.128.128:8083'))
        .pipe(zip('EditorUI_Package.zip'))
        .pipe(gulp.dest('./Packages'));
}

gulp.task('packViewer', prepareViewerUIPackage);
gulp.task('packEditor', prepareEditorUIPackage);

gulp.task('pack', function() {
    prepareViewerUIPackage();
    prepareEditorUIPackage();
});