Write-Host "Dropping the DB" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"

$command = { Invoke-Sqlcmd -ServerInstance "(localdb)\MSSQLLocalDb" -InputFile "C:\packages\dbDrop.sql" }
Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor Green