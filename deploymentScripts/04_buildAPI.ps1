Write-Host "Running dotnet publish" -ForegroundColor White

$currentDir = Get-Location
cd "C:\Coding\Meaningful Projects\MyPlaces\repo\API\API"
dotnet publish -c Release -o "C:\Coding\Meaningful Projects\MyPlaces\Published\API"
cd $currentDir

Write-Host "Done" -ForegroundColor Green

Write-Host "Compressing files" -ForegroundColor White
Compress-Archive -Path "C:\Coding\Meaningful Projects\MyPlaces\Published\API\*" -Update -CompressionLevel Optimal -DestinationPath "C:\Coding\Meaningful Projects\MyPlaces\Published\package_api.zip"
Write-Host "Done" -ForegroundColor Green