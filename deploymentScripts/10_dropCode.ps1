Write-Host "Removing old ugly code" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"

$command = {
    # In case Powershell won't be able to remove the files, try solutions proposed at https://serverfault.com/questions/199921/force-remove-files-and-directories-in-powershell-fails-sometimes-but-not-always
    Remove-Item "C:\MyPlaces\EditorAPI" -Recurse -Force
    #cmd.exe /c rd /s /q "C:\MyPlaces\EditorAPI"
    New-Item -Path "C:\MyPlaces\EditorAPI" -ItemType Directory

    Remove-Item "C:\MyPlaces\EditorUI" -Recurse -Force
    New-Item -Path "C:\MyPlaces\EditorUI" -ItemType Directory

    Remove-Item "C:\MyPlaces\ViewerUI" -Recurse -Force
    New-Item -Path "C:\MyPlaces\ViewerUI" -ItemType Directory
}

Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor Green