Write-Host "Copying new shiny code" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"

$command = {
    Expand-Archive -LiteralPath "C:\packages\package_api.zip" -DestinationPath "C:\MyPlaces\EditorAPI"
    Expand-Archive -LiteralPath "C:\packages\package_view.zip" -DestinationPath "C:\MyPlaces\ViewerUI"
    Expand-Archive -LiteralPath "C:\packages\package_edit.zip" -DestinationPath "C:\MyPlaces\EditorUI"
}

Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor Green
