Write-Host "Creating DB Backup" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"

$createFolderCommand = {
    $date = Get-Date    

    $Folder = "C:\DbBackups\" + $date.ToString("dd-MM-yyyy_HHmm")
    $FileName = $Folder + "\myplaces.bak"

    if (!(Test-Path $Folder))
    {
        New-Item -ItemType Directory -Path $Folder
    }

    if (Test-Path $FileName)
    {
        Remove-Item $FileName
    }
}

Invoke-Command -Session $remoteSession -ScriptBlock $createFolderCommand

$createBackupCommand = { Invoke-Sqlcmd -ServerInstance "(localdb)\MSSQLLocalDb" -InputFile "C:\packages\dbBackup.sql" }
Invoke-Command -Session $remoteSession -ScriptBlock $createBackupCommand

$copyCommand = { Copy-Item "C:\DbBackups\myplaces.bak" -Destination $FileName }
Invoke-Command -Session $remoteSession -ScriptBlock $copyCommand

$removeCommand = { Remove-Item "C:\DbBackups\myplaces.bak" }
Invoke-Command -Session $remoteSession -ScriptBlock $removeCommand

Write-Host "Done" -ForegroundColor Green