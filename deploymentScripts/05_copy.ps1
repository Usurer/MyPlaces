$remoteSession = Get-PSSession -Name "MPdeployment"

Write-Host "Copying packages" -ForegroundColor White
Copy-Item "C:\Coding\Meaningful Projects\MyPlaces\Published\package_api.zip" -Destination "C:\packages\package_api.zip" -ToSession $remoteSession
Copy-Item "C:\Coding\Meaningful Projects\MyPlaces\Published\package_edit.zip" -Destination "C:\packages\package_edit.zip" -ToSession $remoteSession
Copy-Item "C:\Coding\Meaningful Projects\MyPlaces\Published\package_view.zip" -Destination "C:\packages\package_view.zip" -ToSession $remoteSession
Write-Host "Done" -ForegroundColor Green

Write-Host "Copying scripts" -ForegroundColor White
Copy-Item "C:\Coding\Meaningful Projects\MyPlaces\repo\deploymentScripts\*.sql" -Destination "C:\packages" -ToSession $remoteSession
Write-Host "Done" -ForegroundColor Green