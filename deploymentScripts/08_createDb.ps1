Write-Host "Creating the DB" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"

$createFolderCommand = {
    $Folder = "C:\DB"
    $FileName = $Folder + "\myplaces.bak"

    if (!(Test-Path $Folder))
    {
        New-Item -ItemType Directory -Path $Folder
    }

    if (Test-Path $FileName)
    {
        Remove-Item $FileName
    }
}

Invoke-Command -Session $remoteSession -ScriptBlock $createFolderCommand

$command = { Invoke-Sqlcmd -ServerInstance "(localdb)\MSSQLLocalDb" -InputFile "C:\packages\dbCreate.sql" }
Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor White