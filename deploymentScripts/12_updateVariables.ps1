Write-Host "Changing API url" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"
$command = {
    $content = Get-Content "C:\MyPlaces\EditorUI\scripts\MyPlaces.js" | ForEach-Object { $_ -replace "http://localhost:8081", "http://ec2-18-188-51-206.us-east-2.compute.amazonaws.com:8083" }
    Set-Content "C:\MyPlaces\EditorUI\scripts\MyPlaces.js" $content

    $content = Get-Content "C:\MyPlaces\ViewerUI\scripts\MyPlaces.js" | ForEach-Object { $_ -replace "http://localhost:8081", "http://ec2-18-188-51-206.us-east-2.compute.amazonaws.com:8084" }
    Set-Content "C:\MyPlaces\ViewerUI\scripts\MyPlaces.js" $content
 }
Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor Green