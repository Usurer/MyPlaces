Alas, it's impossible to use varibles for DB name or file paths.
Known solutions use sql template string with further replacement of placeholders and running exec (@query) to execute the script.
Interesting possibilities are provided by using SQLCMD with scripting variables, see:
https://docs.microsoft.com/en-us/sql/relational-databases/scripting/sqlcmd-use-with-scripting-variables