Write-Host "Creating DB tables" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"

$command = { Invoke-Sqlcmd -ServerInstance "(localdb)\MSSQLLocalDb" -InputFile "C:\packages\migrations.sql" }
Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor White