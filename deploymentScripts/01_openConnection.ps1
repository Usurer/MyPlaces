# Have no idea why I just don't run Get-PSSession -Name "MPdeployment" and check the result for NULL
foreach ($session in Get-PSSession) {
    if ($session.Name -eq "MPdeployment") {
        Write-Host "Existing session was found." -ForegroundColor White
        return
    }
}

Write-Host "Connecting to remote host. You'll be asked for a password now." -ForegroundColor White
$session = New-PSSession -ComputerName ec2-18-188-51-206.us-east-2.compute.amazonaws.com -Name "MPdeployment" -UseSSL -Credential Administrator