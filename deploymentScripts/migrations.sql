﻿USE myplaces
GO

IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Shapes] (
    [Id] int NOT NULL IDENTITY,
    [ShapeType] nvarchar(max) NULL,
    [Description] nvarchar(max) NULL,
    [FillColor] nvarchar(max) NULL,
    [Name] nvarchar(max) NULL,
    [ParentId] int NULL,
    [Path] nvarchar(max) NULL,
    [Position] nvarchar(max) NULL,
    [StrokeColor] nvarchar(max) NULL,
    CONSTRAINT [PK_Shapes] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Shapes_Shapes_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [Shapes] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_Shapes_ParentId] ON [Shapes] ([ParentId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180107203432_InitialCreate', N'2.0.2-rtm-10011');

GO

ALTER TABLE Shapes  
	ADD Coords geography

GO

--Assume that bounds is a poly and coords are in the following format
 --60.028 30.0762, 60.0405 30.4312, 59.8735 30.4916, 59.8604 30.1181, 60.028 30.0762
CREATE PROCEDURE FindShapesInsideBounds
    @bounds nvarchar(max)
AS   
    SET NOCOUNT ON;  
	
	DECLARE @boundBox geography = geography::STGeomFromText('POLYGON((' + @bounds + '))', 4326).MakeValid();

	SELECT Id
		,ShapeType
		,Description
		,FillColor
		,Name
		,ParentId
		,Path
		,Position
		,StrokeColor
	FROM Shapes
	WHERE @boundBox.STIntersects(Coords) = 1

GO

/*
	EF Core 2.0 doesn't support spatial data types, thus I can't Read or Update spatial data.
	As a workaround I'm gonna keep current Path column to store coords string, while spatial column
	should be used to filter shapes by coords (FindShapesInsideBounds sp).
	To write and update spatial column data I'm gonna use separate stored procedure. Although I may as well
	use an update trigger, triggers are easy to forget about, and, on this project stage, one more DB
	call from the back end won't be that terrible.
*/


 
 -- Assume that bounds is a poly and coords are in the following format
 -- 60.028 30.0762, 60.0405 30.4312, 59.8735 30.4916, 59.8604 30.1181, 60.028 30.0762
CREATE PROCEDURE UpdateCoords
    @shapeId int, 
	@coords nvarchar(max)
AS   
    SET NOCOUNT ON;  
	
	DECLARE @geo geography
	DECLARE @shapeType nvarchar(10)

	SELECT @shapeType = LOWER(ShapeType) FROM Shapes WHERE Id = @shapeId

	SELECT @geo = CASE @shapeType
		WHEN 'line' THEN geography::STGeomFromText('LINESTRING(' + @coords + ')', 4326)
		WHEN 'polygon' THEN geography::STGeomFromText('POLYGON((' + @coords + '))', 4326)
		WHEN 'marker' THEN geography::STGeomFromText('POINT(' + @coords + ')', 4326)
		ELSE NULL
		END

	UPDATE Shapes
	SET Coords = @geo
	WHERE Id = @shapeId

GO

CREATE PROCEDURE GetLength
	@shapeId int
AS
	SET NOCOUNT ON

	SELECT Coords.STLength() as [Length] FROM Shapes WHERE Id = @shapeId

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180121131709_AddSpatialData', N'2.0.2-rtm-10011');

GO

