Clear-Host

Write-Host "Starting deployment" -ForegroundColor White

.\01_openConnection.ps1
.\02_stopIIS.ps1
.\03_buildStatic.ps1
.\04_buildAPI.ps1
.\05_copy.ps1
.\06_backupProdDb.ps1
.\07_dropProdDb.ps1
.\08_createDb.ps1
.\09_updateDb.ps1
.\10_dropCode.ps1
.\11_copyCode.ps1
.\12_updateVariables.ps1
.\13_startIIS.ps1

Write-Host "Deployment complete" -ForegroundColor Green