Write-Host "Running Gulp tasks for static sites" -ForegroundColor White
Start-Process -FilePath "C:\Coding\Meaningful Projects\MyPlaces\repo\WebFront\gulp_build.bat"
Write-Host "Done" -ForegroundColor Green

$editPath = "C:\Coding\Meaningful Projects\MyPlaces\repo\WebFront\Edit\*"
$viewPath = "C:\Coding\Meaningful Projects\MyPlaces\repo\WebFront\View\*"

Write-Host "Compressing files" -ForegroundColor White

if (Test-Path "C:\Coding\Meaningful Projects\MyPlaces\Published\package_edit.zip")
{
    Remove-Item "C:\Coding\Meaningful Projects\MyPlaces\Published\package_edit.zip"
}
if (Test-Path "C:\Coding\Meaningful Projects\MyPlaces\Published\package_view.zip")
{
    Remove-Item "C:\Coding\Meaningful Projects\MyPlaces\Published\package_view.zip"
}   

Compress-Archive -Path $editPath -Update -CompressionLevel Optimal -DestinationPath "C:\Coding\Meaningful Projects\MyPlaces\Published\package_edit.zip"
Compress-Archive -Path $viewPath -Update -CompressionLevel Optimal -DestinationPath "C:\Coding\Meaningful Projects\MyPlaces\Published\package_view.zip"

Write-Host "Done" -ForegroundColor Green