Write-Host "Stopping all sites" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"
$command = { Get-IISSite | Stop-IISSite -Confirm:$false }
Invoke-Command -Session $remoteSession -ScriptBlock $command

$command = { Get-IISAppPool | Stop-WebAppPool }
Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor Green