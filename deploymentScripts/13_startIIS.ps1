Write-Host "Starting all sites" -ForegroundColor White

$remoteSession = Get-PSSession -Name "MPdeployment"
$command = { Get-IISSite | Start-IISSite }
Invoke-Command -Session $remoteSession -ScriptBlock $command

$command = { Get-IISAppPool | Start-WebAppPool }
Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor Green

Write-Host "Running checks" -ForegroundColor White

$command = {
    Write-Host "Testing Editor API at 8083" -ForegroundColor White
    if ((Invoke-WebRequest 'http://localhost:8083/api/shapes/test').StatusCode -ne 200) {
        Write-Host "Editor API site doesn't work" -ForegroundColor Red
    }

    Write-Host "Testing Viewer API at 8084" -ForegroundColor White
    if ((Invoke-WebRequest 'http://localhost:8084/api/shapes/test').StatusCode -ne 200) {
        Write-Host "Viewer API site doesn't work" -ForegroundColor Red
    }

    Write-Host "Testing Editor UI at 8081" -ForegroundColor White
    if ((Invoke-WebRequest 'http://localhost:8081').StatusCode -ne 200) {
        Write-Host "Editor UI site doesn't work" -ForegroundColor Red
    }

    Write-Host "Testing Viewer UI at 8082" -ForegroundColor White
    if ((Invoke-WebRequest 'http://localhost:8082').StatusCode -ne 200) {
        Write-Host "Viewer UI site doesn't work" -ForegroundColor Red
    }
}
Invoke-Command -Session $remoteSession -ScriptBlock $command

Write-Host "Done" -ForegroundColor Green