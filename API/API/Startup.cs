﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using API.DB;
using API.Middleware;
using API.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // Optional.
        // Called by the web host before the Configure method to configure the app's services.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>{});

            services.AddLogging();
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            services.AddMvc();
            services.AddDbContext<ApiDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("ApiDb")));

            services.AddScoped<IDbService, DbService>();
            services.AddScoped<IFilesService, FilesService>();
            services.AddScoped<IKmlService, KmlService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";
                    var logger = NLog.LogManager.GetLogger("Middleware Logger");

                    var error = context.Features.Get<IExceptionHandlerFeature>();
                    if (error != null)
                    {
                        var ex = error.Error;
                        logger.Error(ex);
                        await context.Response.WriteAsync(ex.Message, Encoding.UTF8);
                    }
                });
            });

            app.UseCors("CorsPolicy");

            app.UseCachedImages();

            app.UseMvc();
        }
    }
}
