﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.DB.Models;

namespace API.Utils
{
    public static class ShapeCollectionHelper
    {
        public static (Shape[] added, Shape[] removed, Shape[] changed) UpdateCollection(Shape[] initial, Shape[] modified)
        {
            initial = initial.OrderBy(x => x.Id).ToArray();
            modified = modified.OrderBy(x => x.Id).ToArray();
            
            var added = modified.Where(x => !initial.Any(y => y.Id.Equals(x.Id))).ToList();
            var removed = initial.Where(x => !modified.Any(y => y.Id.Equals(x.Id))).ToList();

            var changedCandidates = initial.Where(x => modified.Any(y => y.Id.Equals(x.Id)));
            var changed = new List<Shape>();

            foreach (var shape in changedCandidates)
            {
                var modifiedShape = modified.Single(x => x.Id.Equals(shape.Id));
                if (AreSimilar(shape, modifiedShape))
                {
                    continue;
                }

                shape.Name = modifiedShape.Name;
                shape.Description = modifiedShape.Description;
                shape.StrokeColor = modifiedShape.StrokeColor;
                shape.FillColor = modifiedShape.FillColor;
                shape.Path = modifiedShape.Path;
                shape.Position = modifiedShape.Position;

                changed.Add(shape);
            }

            return (added.ToArray(), removed.ToArray(), changed.ToArray());
        }

        public static bool AreSimilar(Shape[] a, Shape[] b)
        {
            if (a.Length != b.Length)
            {
                return false;
            }

            a = a.OrderBy(x => x.Id).ToArray();
            b = b.OrderBy(x => x.Id).ToArray();

            for (var i = 0; i < a.Length; i++)
            {
                if (!AreSimilar(a[i], b[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool AreSimilar(Shape a, Shape b)
        {
            return a.Id.Equals(b.Id)
                    && a.Name.Equals(b.Name, StringComparison.InvariantCulture)
                    && a.Description.Equals(b.Description, StringComparison.InvariantCulture)
                    && a.StrokeColor.Equals(b.StrokeColor, StringComparison.InvariantCulture)
                    && a.FillColor.Equals(b.FillColor, StringComparison.InvariantCulture)
                    && a.Path.Equals(b.Path, StringComparison.InvariantCulture)
                    && a.Position.Equals(b.Position, StringComparison.InvariantCulture);
        }
    }
}
