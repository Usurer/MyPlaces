﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.DB.Models
{
    public class Shape
    {
        // TODO: Separate Id and Clustering Index column (https://stackoverflow.com/questions/11938044/what-are-the-best-practices-for-using-a-guid-as-a-primary-key-specifically-rega)
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string ShapeType { get; set; }
        public string StrokeColor { get; set; }
        public string FillColor { get; set; }

        // LatLon Array
        // Line and Polygon have no Position, but Path
        // Mind that SQL server has functions to work with JSON and, maybe, it's possible to use kinda typed data here 
        public string Path { get; set; }

        // LatLon pair
        // Marker has no path, but only postion
        public string Position { get; set; }

        [ForeignKey("Parent")]
        public int? ParentId { get; set; }

        public Shape Parent { get; set; }

        [InverseProperty("Parent")]
        public IEnumerable<Shape> Children { get; set; }
    }
}