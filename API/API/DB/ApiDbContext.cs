﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.DB.Models;
using Microsoft.EntityFrameworkCore;

namespace API.DB
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
        {
        }

        public DbSet<Shape> Shapes { get; set; }
    }
}
