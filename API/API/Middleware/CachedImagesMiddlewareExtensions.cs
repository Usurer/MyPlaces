﻿using Microsoft.AspNetCore.Builder;

namespace API.Middleware
{
    public static class CachedImagesMiddlewareExtensions
    {
        public static IApplicationBuilder UseCachedImages(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CachedImagesMiddleware>();
        }
    }
}