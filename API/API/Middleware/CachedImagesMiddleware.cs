using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace API.Middleware
{
    public class CachedImagesMiddleware
    {
        private readonly RequestDelegate _next;

        public CachedImagesMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IFilesService filesService)
        {
            var match = Regex.Match(context.Request.Path, "/api/Images/shape/(?<shapeId>([0-9])*)/file/(?<fileName>.*)/width/(?<width>([0-9])*)/height/(?<height>([0-9])*)");
            if (!match.Success)
            {
                await _next.Invoke(context);
                return;
            }

            var shapeId = match.Groups["shapeId"].Value;
            var fileName = match.Groups["fileName"].Value;
            var width = int.Parse(match.Groups["width"].Value);
            var height = int.Parse(match.Groups["height"].Value);

            if (filesService.CheckImageCache(fileName, int.Parse(shapeId), width, height))
            {
                var file = filesService.GetImageFromCache(int.Parse(shapeId), fileName, width, height);
                var len = file.Length;
                file.Seek(0, SeekOrigin.Begin);

                using (var readStream = file)
                {
                    context.Response.ContentType = "image/jpeg";
                    context.Response.ContentLength = len;

                    // Shamelessly taken from StaticFileMiddleware https://github.com/aspnet/StaticFiles
                    await StreamCopyOperation.CopyToAsync(readStream, context.Response.Body, len, 64 * 1024, context.RequestAborted);
                }
            }
            else
            {
                await _next.Invoke(context);
            }
        }
    }
}