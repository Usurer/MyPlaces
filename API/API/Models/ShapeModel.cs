﻿using System;

namespace API.Models
{
public class ShapeModel
    {
        public ShapeModel()
        {
            Path = Array.Empty<Coords>();
            Children = Array.Empty<ShapeModel>();
        }

        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ShapeType { get; set; }

        public string StrokeColor { get; set; }

        public string FillColor { get; set; }

        public Coords[] Path { get; set; }

        public Coords Position { get; set; }

        public ShapeModel[] Children { get; set; }

        public int? ParentId { get; set; }
    }
}
