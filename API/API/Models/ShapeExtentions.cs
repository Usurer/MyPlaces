﻿using System;
using System.Linq;
using API.DB;
using API.DB.Models;
using Newtonsoft.Json;

namespace API.Models
{
    public static class ShapeExtentions
    {
        public static Shape FromModel(this ShapeModel model)
        {
            return new Shape
            {
                Path = JsonConvert.SerializeObject(model.Path),
                Position = JsonConvert.SerializeObject(model.Position),
                ShapeType = model.ShapeType,
                StrokeColor = model.StrokeColor,
                FillColor = model.FillColor,
                Description = model.Description,
                Name = model.Name,
                // Children should be ICollection because of EF restrictions
                Children = model.Children.Select(x => x.FromModel()).ToList(),
            };
        }

        public static ShapeModel ToModel(this Shape shape)
        {
            return new ShapeModel
            {
                Id = shape.Id,
                Name = shape.Name,
                Description = shape.Description,
                ShapeType = shape.ShapeType,
                StrokeColor = shape.StrokeColor,
                FillColor = shape.FillColor,
                Position = JsonConvert.DeserializeObject<Coords>(shape.Position),
                Path = JsonConvert.DeserializeObject<Coords[]>(shape.Path),
            };
        }
    }
}