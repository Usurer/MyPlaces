﻿using System.IO;
using System.Linq;
using API.Services;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class ImagesController : Controller
    {
        public ImagesController(IFilesService filesService, IDbService dbService)
        {
            FilesService = filesService;
            DbService = dbService;
        }

        private IFilesService FilesService { get; }
        private IDbService DbService { get; }

        [HttpGet("shape/{shapeId}/file/{fileName}", Name = "ShapeImage")]
        public FileStreamResult Get(int shapeId, string fileName)
        {
            /*  This method is mostly a placeholder - IRL I don't want to get images without sizes, but having this method is useful because
                I can get URL using the route, as done at GetImagesForShape() */
            return Get(shapeId, fileName, 200, 200);
        }

        // TODO: Move all resizing logic to a new Service
        [HttpGet("shape/{shapeId}/file/{fileName}/width/{width}/height/{height}")]
        public FileStreamResult Get(int shapeId, string fileName, int width, int height)
        {
            using (var s = FilesService.GetImage(shapeId, fileName))
            {
                using (var image = Image.Load(s))
                {
                    var options = new ResizeOptions
                    {
                        Mode = ResizeMode.Crop
                    };

                    var size = new Size
                    {
                        Height = height,
                        Width = width
                    };

                    options.Size = size;

                    var clone = image.Clone(x => x.Resize(options));
                    var stream = new MemoryStream();

                    clone.SaveAsJpeg(stream);

                    // Move stream pointer to the beginning of the stream - otherwise it will return 0 bytes.
                    stream.Seek(0, SeekOrigin.Begin);
                    FilesService.SaveImageToCache(stream, fileName, shapeId, size.Width, size.Height);
                    
                    return new FileStreamResult(stream, "image/jpeg");
                }
            }
        }

        [HttpGet("shape/{shapeId}")]
        public JsonResult GetImagesForShape(int shapeId)
        {
            var names = FilesService.GetImagesNamesForShape(shapeId);
            var model = names.Select(x => new
                {
                    Name = x,
                    Src = Url.RouteUrl("ShapeImage", new {shapeId, fileName = x}, "http")
                })
                .ToArray();

            return new JsonResult(model);
        }

        private static string GetMediaTypeByName(string fileName)
        {
            var dotIndex = fileName.LastIndexOf('.');
            var extension = fileName.Substring(dotIndex + 1);

            switch (extension)
            {
                case "jpg":
                    return "image/jpeg";
                case "jpeg":
                    return "image/jpeg";
                case "png":
                    return "image/png";
                default:
                    return "image";
            }
        }
    }
}