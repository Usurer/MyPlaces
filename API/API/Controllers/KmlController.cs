﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class KmlController : Controller
    {
        private IKmlService KmlService { get; set; }

        private IDbService DbService { get; set; }

        public KmlController(IKmlService kmlService, IDbService dbService)
        {
            KmlService = kmlService;
            DbService = dbService;
        }

        //public JsonResult Post(IFormFile file)
        //{
        //    var stream = file.OpenReadStream();
        //    var features = KmlService.Parse(stream); 

        //    return new JsonResult(features);
        //}

        public JsonResult Post(IFormFile file)
        {
            var stream = file.OpenReadStream();
            var shapes = KmlService.ToShapes(stream);

            foreach (var model in shapes)
            {
                var entity = DbService.Create(model);
                model.Id = entity.Id;
            }
            
            return new JsonResult(shapes);
        }
    }
}
