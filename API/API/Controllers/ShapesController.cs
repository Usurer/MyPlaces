﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Models;
using API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class ShapesController : Controller
    {
        private IDbService DbService { get; set; }
        private IFilesService FilesService { get; set; }

        public ShapesController(IDbService dbService, IFilesService filesService)
        {
            DbService = dbService;
            FilesService = filesService;
        }

        [HttpGet]
        [Route("Test")]
        public JsonResult Test()
        {
            return new JsonResult("Test");
        }

        [HttpGet]
        public JsonResult Get()
        {
            var individualShapes = DbService.Read().Where(x => !x.ParentId.HasValue).ToArray();
            var result = new List<ShapeModel>();
            foreach (var s in individualShapes)
            {
                // TODO: Is it possible for children to have children?
                var children = DbService.Read().Where(x => x.ParentId == s.Id).Select(x => x.ToModel()).ToArray();
                var model = s.ToModel();
                model.Children = children;
                result.Add(model);
            }

            return new JsonResult(result);
        }

        [HttpGet("{top}/{bottom}/{left}/{right}")]
        public JsonResult Get(decimal top, decimal bottom, decimal left, decimal right)
        {
            var boundaries = new Coords[]
            {
                new Coords {Lat = top, Lng = right},
                new Coords {Lat = top, Lng = left},
                new Coords {Lat = bottom, Lng = left},
                new Coords {Lat = bottom, Lng = right},
                new Coords {Lat = top, Lng = right},
            };

            var individualShapes = DbService.Read(boundaries).Where(x => !x.ParentId.HasValue).ToArray();
            var result = new List<ShapeModel>();
            foreach (var s in individualShapes)
            {
                // TODO: Is it possible for children to have children?
                var children = DbService.Read().Where(x => x.ParentId == s.Id).Select(x => x.ToModel()).ToArray();
                var model = s.ToModel();
                model.Children = children;
                result.Add(model);
            }

            return new JsonResult(result);
        }

        [HttpGet("details/{id}")]
        public JsonResult GetDetails(int id)
        {
            var length = DbService.GetLength(id);
            var result = (length / 1000)?.ToString("F2");

            return new JsonResult(result);
        }

        [HttpPost]
        public int Post(string shape, string removedFilesNames, List<IFormFile> newFiles)
        {
            var shapeModel = JsonConvert.DeserializeObject<ShapeModel>(shape);
            var exists = shapeModel.Id.HasValue && DbService.Read(shapeModel.Id.Value) != null;

            var shapeId = exists 
                ? DbService.Update(shapeModel).Id 
                : DbService.Create(shapeModel).Id;

            foreach (var formFile in newFiles)
            {
                FilesService.SaveImage(formFile, shapeId);
            }

            if(!string.IsNullOrEmpty(removedFilesNames))
            {
                foreach (var name in JsonConvert.DeserializeObject<IEnumerable<string>>(removedFilesNames))
                {
                    FilesService.DeleteImage(name, shapeId);
                }
            }

            return shapeId;
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            DbService.Delete(id);
        }

        //[HttpDelete]
        //public void Delete()
        //{
        //    DbService.ClearDb();
        //}
    }
}
