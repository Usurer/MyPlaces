﻿// <auto-generated />
using API.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace API.Migrations
{
    [DbContext(typeof(ApiDbContext))]
    [Migration("20180107203432_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("API.DB.Models.Shape", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ShapeType");

                    b.Property<string>("Description");

                    b.Property<string>("FillColor");

                    b.Property<string>("Name");

                    b.Property<int?>("ParentId");

                    b.Property<string>("Path");

                    b.Property<string>("Position");

                    b.Property<string>("StrokeColor");

                    b.HasKey("Id");

                    b.HasIndex("ParentId");

                    b.ToTable("Shapes");
                });

            modelBuilder.Entity("API.DB.Models.Shape", b =>
                {
                    b.HasOne("API.DB.Models.Shape", "Parent")
                        .WithMany("Children")
                        .HasForeignKey("ParentId");
                });
#pragma warning restore 612, 618
        }
    }
}
