﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.IO;

namespace API.Migrations
{
    public partial class AddSpatialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var script = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Migrations", "addSpatial_up.sql"));
            migrationBuilder.Sql(script);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var script = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Migrations", "addSpatial_down.sql"));
            migrationBuilder.Sql(script);
        }
    }
}
