﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services
{
    public class KmlPlottable
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public List<KmlGeometry> Geometries { get; set; }

        public List<KmlPlottable> Children { get; set; }
    }

    public class KmlGeometry
    {
        public List<Coords> Coords { get; set; }

        public List<KmlGeometry> Children { get; set; }
    }


}
