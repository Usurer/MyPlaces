﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API.DB;
using API.DB.Models;
using API.Models;
using API.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using Newtonsoft.Json;

namespace API.Services
{
    public interface IDbService
    {
        Shape Create(ShapeModel model);

        List<Shape> Read();

        Shape Read(int id);

        List<Shape> Read(Coords[] boundaries);

        Shape Update(ShapeModel model);

        void Delete(int id);

        void ClearDb();

        double? GetLength(int id);
    }

    public class DbService : IDbService
    {
        private readonly ApiDbContext _dbContext;

        public DbService(ApiDbContext apiDbContext)
        {
            _dbContext = apiDbContext;
        }

        public Shape Create(ShapeModel model)
        {
            var entity = model.FromModel();
            if (model.ParentId.HasValue)
            {
                var parent = _dbContext.Shapes.Single(x => x.Id.Equals(model.ParentId));
                entity.ParentId = parent.Id;
            }

            foreach (var child in entity.Children)
            {
                child.Parent = entity;
            }

            // It's interesting, but it seems that there's no need to implicitly add children to Shapes,
            // as soon as they're already added to Children collection.
            _dbContext.Shapes.Add(entity);

            _dbContext.SaveChanges(true);

            var coordsString = model.Path != null && model.Path.Any()
                ? string.Join(',', model.Path.Select(x => $"{x.Lng} {x.Lat}"))
                : $"{model.Position.Lng} {model.Position.Lat}";
            _dbContext.Database.ExecuteSqlCommand("exec UpdateCoords {0}, {1}", entity.Id, coordsString);

            return entity;
        }

        public List<Shape> Read()
        {
            return _dbContext.Shapes.ToList();
        }

        public List<Shape> Read(Coords[] boundaries)
        {
            var geoString = string.Join(',', boundaries.Select(x => $"{x.Lng:F4} {x.Lat:F4}"));
            var result = _dbContext.Shapes.FromSql("exec FindShapesInsideBounds {0}", geoString).ToList();
            return result;
        }

        public Shape Update(ShapeModel model)
        {
            var shape = _dbContext.Shapes.Single(x => x.Id.Equals(model.Id));
            shape.Description = model.Description;
            shape.Name = model.Name;
            shape.Path = JsonConvert.SerializeObject(model.Path);
            shape.Position = JsonConvert.SerializeObject(model.Position);
            shape.StrokeColor = model.StrokeColor;
            shape.FillColor = model.FillColor;

            var children = model.Children.Select(x => x.FromModel()).ToArray();
            UpdateChildren(_dbContext, shape.Id, children);

            _dbContext.SaveChanges();

            var coordsString = model.Path.Any() 
                ? string.Join(',', model.Path.Select(x => $"{x.Lng} {x.Lat}"))
                : $"{model.Position.Lat} {model.Position.Lng}";
            _dbContext.Database.ExecuteSqlCommand("exec UpdateCoords {0}, {1}", shape.Id, coordsString);

            return shape;
        }

        private void UpdateChildren(ApiDbContext context, int parentId, Shape[] newChildren)
        {
            var existingChildren = context.Shapes.Where(x => x.ParentId == parentId).ToArray();
            var candidates = ShapeCollectionHelper.UpdateCollection(existingChildren, newChildren);
            context.Shapes.RemoveRange(candidates.removed);

            foreach (var shape in candidates.added)
            {
                shape.ParentId = parentId;
            }
            context.Shapes.AddRange(candidates.added);
        }

        public void Delete(int id)
        {
            var shape = _dbContext.Shapes.SingleOrDefault(x => x.Id.Equals(id));
            if (shape != null)
            {
                if (shape.Children != null)
                {
                    _dbContext.Shapes.RemoveRange(shape.Children);
                }
                _dbContext.Shapes.Remove(shape);
            }
            _dbContext.SaveChanges();
        }

        public Shape Read(int id)
        {
            var shape = _dbContext.Shapes.SingleOrDefault(x => x.Id.Equals(id));
            if (shape == null)
            {
                return null;
            }

            _dbContext.Entry(shape).Collection(x => x.Children).Load();

            return shape;
        }

        public void ClearDb()
        {
            var allShapes = _dbContext.Shapes.Select(x => x);
            _dbContext.Shapes.RemoveRange(allShapes);
            _dbContext.SaveChanges(true);
        }

        public double? GetLength(int id)
        {
            using (var cmd  = _dbContext.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "exec GetLength @shapeId";

                var param = new SqlParameter
                {
                    Value = id,
                    ParameterName = "@shapeId"
                };
                cmd.Parameters.Add(param);

                _dbContext.Database.OpenConnection();
                var result = cmd.ExecuteScalar();
                return Convert.IsDBNull(result) 
                    ? null 
                    : (double?) result;
            }
        }
    }
}
