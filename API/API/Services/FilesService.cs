﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Storage.Internal;

namespace API.Services
{
    public interface IFilesService
    {
        void SaveImage(IFormFile file, int shapeId);

        void SaveImageToCache(Stream file, string fileName, int shapeId, int width, int height);

        void DeleteImage(string imageId, int shapeId);

        Stream GetImage(int shapeId, string fileName);

        Stream GetImageFromCache(int shapeId, string fileName, int width, int height);

        bool CheckImageCache(string fileName, int shapeId, int width, int height);

        string[] GetImagesNamesForShape(int shapeId);
    }

    public class FilesService : IFilesService
    {
        private IHostingEnvironment HostingEnvironment { get; set; }

        private const string ImgPath = "Images";
        private const string CachePath = "Cache";

        public FilesService(IHostingEnvironment hostingEnvironment)
        {
            HostingEnvironment = hostingEnvironment;
        }

        private string GetImagesFolder(int shapeId)
        {
            var root = HostingEnvironment.ContentRootPath;
            return Path.Combine(root, ImgPath, shapeId.ToString());
        }

        private string GetImagesCacheFolder(int shapeId)
        {
            var root = HostingEnvironment.ContentRootPath;
            return Path.Combine(root, CachePath, shapeId.ToString());
        }

        private string GetImagesCacheFolderPath(int shapeId, int width, int height)
        {
            return Path.Combine(GetImagesCacheFolder(shapeId), $"{width}_{height}");
        }

        private string GetFilePath(int shapeId, string fileName)
        {
            return Path.Combine(GetImagesFolder(shapeId), fileName);
        }

        public Stream GetImageFromCache(int shapeId, string fileName, int width, int height)
        {
            return File.OpenRead(Path.Combine(GetImagesCacheFolderPath(shapeId, width, height), fileName));
        }

        public bool CheckImageCache(string fileName, int shapeId, int width, int height)
        {
            var folder = GetImagesCacheFolder(shapeId);
            if (Directory.Exists(folder))
            {
                if (File.Exists(Path.Combine(GetImagesCacheFolderPath(shapeId, width, height), fileName)))
                {
                    return true;
                }
            }

            return false;
        }

        public string[] GetImagesNamesForShape(int shapeId)
        {
            var path = GetImagesFolder(shapeId);
            if (!Directory.Exists(path))
            {
                return Array.Empty<string>();
            }

            return Directory.EnumerateFiles(path).Select(Path.GetFileName).ToArray();
        }

        public void SaveImage(IFormFile file, int shapeId)
        {
            var path = GetImagesFolder(shapeId);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
            var fullName = Path.Combine(path, fileName);

            if (File.Exists(fullName))
            {
                throw new IOException($"Bad file name {fullName} - possible GUID collision.");
            }

            using (var stream = File.Create(fullName))
            {
                file.CopyTo(stream);
            }
        }

        public void SaveImageToCache(Stream file, string fileName, int shapeId, int width, int height)
        {
            var folder = GetImagesCacheFolderPath(shapeId, width, height);
            var fullPath = Path.Combine(folder, fileName);

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            using (var stream = File.Create(fullPath))
            {
                file.CopyTo(stream);
            }

            // Return to the beginning of source stream
            file.Seek(0, SeekOrigin.Begin);
        }

        public Stream GetImage(int shapeId, string fileName)
        {
            return File.OpenRead(GetFilePath(shapeId, fileName));
        }

        public void DeleteImage(string fileName, int shapeId)
        {
            File.Delete(GetFilePath(shapeId, fileName));
        }
    }
}
