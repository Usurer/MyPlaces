﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using SharpKml;
using SharpKml.Dom;
using Shape = API.DB.Models.Shape;

namespace API.Services
{
    public interface IKmlService
    {
        List<KmlPlottable> Parse(Stream file);

        List<ShapeModel> ToShapes(Stream file);
    }

    public class KmlService : IKmlService
    {
        public List<KmlPlottable> Parse(Stream file)
        {
            var result = new List<KmlPlottable>();

            var parser = new SharpKml.Base.Parser();
            parser.Parse(file);
            var root = (Kml) parser.Root;
            if (root.Feature is Container container)
            {
                result.AddRange(ParseContainer(container));
            }

            return result;
        }

        public List<ShapeModel> ToShapes(Stream file)
        {
            var result = new List<ShapeModel>();
            var plottables = Parse(file);

            foreach (var plottable in plottables)
            {
                var shapeModel = new ShapeModel
                {
                    Name = plottable.Name,
                    Description = plottable.Description,
                };

                shapeModel.StrokeColor = "#000";
                var coords = plottable.Geometries.SelectMany(x => x.Coords).ToArray();
                if (coords.Length == 1)
                {
                    shapeModel.ShapeType = "Marker";
                    shapeModel.Position = coords[0];
                }
                else
                {
                    shapeModel.ShapeType = "Line";
                }

                shapeModel.Path = coords;

                result.Add(shapeModel);
            }

            return result;
        }

        private void ParseOverlay(Overlay overlay)
        {
        }

        private KmlPlottable ParsePlacemark(Placemark placemark)
        {
            var result = new KmlPlottable
            {
                Name = placemark.Name,
                Description = placemark.Description?.Text,
                Geometries = ParseGeometry(placemark.Geometry),
            };
            return result;
        }

        private List<KmlPlottable> ParseContainer(Container container)
        {
            var result = new List<KmlPlottable>();

            foreach (var feature in container.Features)
            {
                if (feature is Placemark placemark)
                {
                    var plottable = ParsePlacemark(placemark);
                    result.Add(plottable);
                }
            }

            return result;

        }

        private List<KmlGeometry> ParseGeometry(Geometry geometry)
        {
            var result = new List<KmlGeometry>();
            if (geometry is Point point)
            {
                var coords = new Coords
                {
                    Lat = (decimal) point.Coordinate.Latitude,
                    Lng = (decimal) point.Coordinate.Longitude,
                };
                result.Add(new KmlGeometry
                {
                    Coords = new List<Coords> { coords }
                });
            }
            else if (geometry is LineString line)
            {
                foreach (var coordinate in line.Coordinates)
                {
                    var coords = new Coords
                    {
                        Lat = (decimal) coordinate.Latitude,
                        Lng = (decimal) coordinate.Longitude,
                    };

                    result.Add(new KmlGeometry
                    {
                        Coords = new List<Coords> { coords}
                    });
                }
            }
            else if (geometry is MultipleGeometry multiGeometry)
            {
                foreach (var subGeo in multiGeometry.Geometry)

                {
                    result.AddRange(ParseGeometry(subGeo));
                }
            }
            return result;
        }

        public IEnumerable<(double Lat, double Lon)> ParseData(Stream file)
        {
            var result = new List<(double Lat, double Lon)>();
            var parser = new SharpKml.Base.Parser();
            parser.Parse(file);
            var root = (Kml)parser.Root; // root is always a KML
            var features = ((SharpKml.Dom.Document) root.Feature).Features;
            foreach (var feature in features)
            {
                if (feature is SharpKml.Dom.Placemark)
                {
                    var placemark = feature as SharpKml.Dom.Placemark;
                    var multipleGeometry = placemark.Geometry as MultipleGeometry;
                    if (multipleGeometry != null)
                    {
                        foreach (var geometry in multipleGeometry.Geometry)
                        {
                            var lineString = geometry as LineString;
                            if (lineString != null)
                            {
                                var coords = lineString.Coordinates;
                                foreach (var coord in coords)
                                {
                                    result.Add((coord.Latitude, coord.Longitude));
                                }
                            }
                        }
                    }

                }
            }

            return result;
        }
    }
}
