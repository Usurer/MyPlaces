﻿namespace API
{
    public class Coords
    {
        public decimal Lat { get; set; }

        public decimal Lng { get; set; }
    }
}