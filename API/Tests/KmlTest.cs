﻿using System.IO;
using API.Services;
using Xunit;
using SharpKml;
using SharpKml.Base;
using SharpKml.Dom;
using SharpKml.Engine;

namespace Tests
{
    public class KmlTest
    {
        [Fact]
        public void BasicParseTest()
        {
            using (var file = File.OpenRead("example.kml"))
            {
                var parser = new Parser();
                parser.Parse(file);
            }
            
        }

        [Fact]
        public void KmlToFeaturesListTest()
        {
            var service = new KmlService();

            using (var file = File.OpenRead("example.kml"))
            {
                var data = service.Parse(file);
                Assert.Equal(21, data.Count);
            }

        }
    }
}