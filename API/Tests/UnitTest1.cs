using System;
using System.Linq;
using API.DB;
using API.DB.Models;
using API.Models;
using API.Services;
using API.Utils;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Sdk;

namespace Tests
{
    public class UnitTest1
    {
        private ShapeModel Parent_1, Parent_2;
        private ShapeModel Child_1, Child_2, Child_3, Child_4, Child_5;

        public UnitTest1()
        {
            Parent_1 = new ShapeModel { Name = "Parent 1", Id = 0, Children = Array.Empty<ShapeModel>()};
            Parent_2 = new ShapeModel { Name = "Parent 2", Id = 0, Children = Array.Empty<ShapeModel>() };

            Child_1 = new ShapeModel { Name = "Child 1", Id = 0, Children = Array.Empty<ShapeModel>() };
            Child_2 = new ShapeModel { Name = "Child 2", Id = 0, Children = Array.Empty<ShapeModel>() };
            Child_3 = new ShapeModel { Name = "Child 3", Id = 0, Children = Array.Empty<ShapeModel>() };
            Child_4 = new ShapeModel { Name = "Child 4", Id = 0, Children = Array.Empty<ShapeModel>() };
            Child_5 = new ShapeModel { Name = "Child 5", Id = 0, Children = Array.Empty<ShapeModel>() };
        }

        private Shape CreateShape(int id, string name, string description)
        {
            return new Shape
            {
                Id = id,
                Name = name,
                Description = description,
                Path = string.Empty,
                Position = string.Empty,
                StrokeColor = string.Empty,
                FillColor = string.Empty,
            };
        }

        [Fact]
        public void ChildrenComparerTest()
        {
            var shape1 = CreateShape(0, "Shape 1 Name", "Shape 1 Description");
            var shape2 = CreateShape(1, "Shape 2 Name", "Shape 2 Description");
            var shape3 = CreateShape(2, "Shape 3 Name", "Shape 3 Description");
            var shape4 = CreateShape(3, "Shape 4 Name", "Shape 4 Description");

            var shape1_1 = CreateShape(0, "Shape 1_1 Name", "Shape 1_1 Description");

            var a = new[] { shape1, shape2, shape3 };
            var b = new[] { shape1_1, shape2, shape4 };

            var result_ab = ShapeCollectionHelper.UpdateCollection(a, b);

            Assert.Equal(1, result_ab.added.Length);
            Assert.Equal(3, result_ab.added[0].Id);

            Assert.Equal(1, result_ab.removed.Length);
            Assert.Equal(2, result_ab.removed[0].Id);

            Assert.Equal(1, result_ab.changed.Length);
            Assert.Equal(0, result_ab.changed[0].Id);
        }

        //[Fact]
        //public void ContextTest()
        //{
        //    var contextOptions = new DbContextOptionsBuilder<ApiDbContext>()
        //        .UseInMemoryDatabase("MyPlaces_test")
        //        .Options;

        //    using (var context = new ApiDbContext(contextOptions))
        //    {
        //        Assert.False(context.Shapes.Any());

        //        context.Shapes.Add(Parent_1.FromModel());
        //        context.SaveChanges();
        //        Assert.True(context.Shapes.Count() == 1);
        //    }
        //}

        //[Fact]
        // TODO: Test won't run because of raw SQL calls (stored procedures)
        public void CrudTest()
        {
            var contextOptions = new DbContextOptionsBuilder<ApiDbContext>()
                .UseInMemoryDatabase("MyPlaces_test")
                .Options;

            using (var context = new ApiDbContext(contextOptions))
            {
                var dbService = new DbService(context);
                dbService.ClearDb();

                Assert.False(context.Shapes.Any());

                Parent_1.Id = dbService.Create(Parent_1).Id;
                Assert.Equal(dbService.Read().Count, 1);

                var parent_1_result = dbService.Read(Parent_1.Id.Value);
                var parent_1_model = parent_1_result.ToModel();
                parent_1_model.Children = new[] { Child_1, Child_2 };
                dbService.Update(parent_1_model);

                parent_1_result = dbService.Read(Parent_1.Id.Value);
                Assert.True(parent_1_result.Children.Count() == 2);
                Assert.NotNull(parent_1_result.Children.SingleOrDefault(x => x.Name.Equals(Child_1.Name)));
                Assert.NotNull(parent_1_result.Children.SingleOrDefault(x => x.Name.Equals(Child_2.Name)));

                dbService.Delete(Parent_1.Id.Value);
                Assert.False(context.Shapes.Any());
            }
        }

        //[Fact]
        // TODO: Test won't run because of raw SQL calls (stored procedures)
        public void CascadeRemovalTest()
        {
            var contextOptions = new DbContextOptionsBuilder<ApiDbContext>()
                .UseInMemoryDatabase("MyPlaces_test")
                .Options;

            using (var context = new ApiDbContext(contextOptions))
            {
                var dbService = new DbService(context);

                Parent_1.Children = new[] { Child_1, Child_2 };
                Parent_1.Id = dbService.Create(Parent_1).Id;

                var parentResult = dbService.Read(Parent_1.Id.Value);

                Assert.Equal(3, dbService.Read().Count);
                Assert.Equal(2, parentResult.Children.Count());

                dbService.Delete(Parent_1.Id.Value);

                Assert.Equal(0, dbService.Read().Count);
            }
        }
    }
}
