MyPlaces.FileUploadController = function FileUploadController() {

    const url = MyPlaces.Environment.apiUrl + '/api/images/';
    const fileInputSelector = 'fileInput';

    this.upload = function(onStart, onEnd, onSuccess) {

        let files = document.getElementById(fileInputSelector).files;
        let formData = new FormData();

        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            formData.append("files", file);
        }

        onStart();
        
        $.ajax(url, {
            method: "POST",
            success: onSuccess,
            complete: onEnd,
            error: function(){ console.log('AJAX Error'); },
            contentType: false,
            processData: false,
            data: formData
        });

    }
}