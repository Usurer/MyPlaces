MyPlaces.Map.ShapeContextMenuController = function ShapeContextMenuController(mapController, shapesOnMap) {
    
    var self = this;
    self.mapController = mapController;
    self.shapesOnMap = shapesOnMap;
    
    

    /**
     * Opens ContextMenu
     * 
     * @param {Object} latLng 
     * @param {Object[]} buttons 
     * @param {Object[]} shapesOnMap IShape objects that are currently present on map
     */
    this.openShapeContextMenu = function openShapeContextMenu(latLng, buttons) {
        if(!isContextMenuAllowed()) {
            return;
        }

        // TODO: We don't really need a new Menu each time we do the click. Think about: menu per shape or one menu per whole app.
        var menu = new MyPlaces.Map.ShapeContextMenu(buttons);
        menu.open(self.mapController.map, latLng);
    }

    // TODO: Won't work with SplitLineEditor, since it doesn't make a map shape editable
    function isContextMenuAllowed() {
        for(let shape of self.shapesOnMap) {
            if(shape[1].getShape().editable) {
                console.log('Context menu in unavailable because of the shape');
                console.info(shape[1]);
                return false;
            }
        }
        
        return true;
    }
}