MyPlaces.Map.ShapeContextMenu = function ShapeContextMenu(menuItemTitleHandlerPairs){

    this.div_ = document.createElement('div');
    this.div_.className = 'map-context-menu';

    this.items = [];
    
    for(var i = 0; i < menuItemTitleHandlerPairs.length; i++) {

        var menuItem = new ContextMenuItem(menuItemTitleHandlerPairs[i].title, menuItemTitleHandlerPairs[i].handler, this);
        this.items.push(menuItem);
        this.div_.appendChild(menuItem.div_);
    }

    function ContextMenuItem(title, clickHandler, parentMenu) {
        this.div_ = document.createElement('div');
        this.div_.innerHTML = title;
        this.div_.className = 'map-context-menu__item';

        google.maps.event.addDomListener(this.div_, 'click', function (event) {
            var latLng = parentMenu.get('position');
            clickHandler(latLng);
            event.stopPropagation();
            parentMenu.close();
        }, true);
    }
    
};

/**
 * Sets prototype for ShapeContextMenu. Listener is required because I need google script to be loaded.
 * Probably can just use document.ready.
 */
document.addEventListener('mapInitComplete', () => { 
    MyPlaces.Map.ShapeContextMenu.prototype = new google.maps.OverlayView();
    
    MyPlaces.Map.ShapeContextMenu.prototype.onAdd = function () {
        var contextMenu = this;
        var map = this.getMap();
        this.getPanes().floatPane.appendChild(this.div_);

        this.divListener_ = google.maps.event.addDomListener(map.getDiv(), 'mousedown', function (e) {
            var targetIsMenuItem = false;
            for(var i = 0; i < contextMenu.items.length; i++) {
                if(e.target === contextMenu.items[i].div_) {
                    targetIsMenuItem = true;
                    break;
                }
            }
            
            if(!targetIsMenuItem) {
                contextMenu.close();
            }

        }, true);

        // It's an interesting thing, that if I won't place 'true' as a capturing param to this handler, handler won't fire on vertex right click.
    };

    MyPlaces.Map.ShapeContextMenu.prototype.onRemove = function () {
        google.maps.event.removeListener(this.divListener_);
        this.div_.parentNode.removeChild(this.div_);

        // clean up
        this.set('position');
        this.set('path');
        this.set('vertex');
    };

    MyPlaces.Map.ShapeContextMenu.prototype.close = function () {
        this.setMap(null);
    };

    MyPlaces.Map.ShapeContextMenu.prototype.draw = function () {
        var position = this.get('position');
        var projection = this.getProjection();

        if (!position || !projection) {
            return;
        }

        var point = projection.fromLatLngToDivPixel(position);
        this.div_.style.top = point.y + 'px';
        this.div_.style.left = point.x + 'px';
    };

    MyPlaces.Map.ShapeContextMenu.prototype.open = function (map, latLng) {
        this.set('position', latLng);
        this.setMap(map);
        
        this.draw;
    };

    MyPlaces.Map.ShapeContextMenu.prototype.removeVertex = function () {
        var path = this.get('path');
        var vertex = this.get('vertex');

        if (!path || vertex == undefined) {
            this.close();
            return;
        }

        path.removeAt(vertex);
        this.close();
    };
})
