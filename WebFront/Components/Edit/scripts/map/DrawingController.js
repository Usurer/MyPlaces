MyPlaces.Map.DrawingController = function MapDrawingController(mapController, rightPanelMenuController) {
    
    // TODO: try to get rid of this pattern just for the sake of curiosity
    const self = this;
    const shapesOnMap = new Map();

    self.mapController = mapController;
    self.shapeContextMenuController = new MyPlaces.Map.ShapeContextMenuController(mapController, shapesOnMap);
    self.currentTool = null;
    self.splitLineEditor;

    function addToShapesCollection(shape) {
        shapesOnMap.set(shape.id, shape);
    }

    function removeFromShapesCollection(shape) {
        shapesOnMap.delete(shape.id);
    }

    this.isOnMap = function(shape) {
        return shapesOnMap.has(shape.id);
    }

    this.cancelCurrentDrawing = function cancelCurrentDrawing() {
        if(self.currentTool) {       
            removeFromShapesCollection(self.currentTool);
            if(self.splitLineEditor) {
                self.splitLineEditor.stopEditing();
                self.splitLineEditor = null;
            }
            
            // In most cases we don't want to modify shape children anyhow.
            self.currentTool.clear(false);
        }

        self.currentTool = null;
    }

    this.enterPolylinePlotting = function enterPolylinePlotting() {
        self.cancelCurrentDrawing();
        self.currentTool = MyPlaces.Shapes.utils.createNewPolyline(mapController.map);
        self.currentTool.setRightClickHandler(openShapeContextMenu);
        self.currentTool.setLeftClickHandler(shapeClickHandler);
        addToShapesCollection(self.currentTool);
        self.currentTool.enableEditHandlers();        
    }

    this.enterPolygonPlotting = function enterPolygonPlotting() {
        self.cancelCurrentDrawing();
        self.currentTool = MyPlaces.Shapes.utils.createNewPolygon(mapController.map);
        self.currentTool.setRightClickHandler(openShapeContextMenu);
        self.currentTool.setLeftClickHandler(shapeClickHandler);
        addToShapesCollection(self.currentTool);
    }

    this.enterMarkerPlotting = function enterMarkerPlotting() {
        self.cancelCurrentDrawing();
        self.currentTool = MyPlaces.Shapes.utils.createNewMarker(mapController.map);
        self.currentTool.setRightClickHandler(openShapeContextMenu);
        self.currentTool.setLeftClickHandler(shapeClickHandler);
        addToShapesCollection(self.currentTool);
    }

    this.enterShapeMarkerPlotting = function enterShapeMarkerPlotting(editMenuCallback) {
        self.cancelCurrentDrawing();
        
        $('.notification.shape-marker').removeClass('d-none');
        MyPlaces.Observer.subscribe('shapeClick', (shape) => {
            $('.notification.shape-marker').addClass('d-none');
            self.currentTool = MyPlaces.Shapes.utils.createNewMarker(mapController.map);
            self.currentTool.parent = shape;
            self.currentTool.setRightClickHandler(openShapeContextMenu);
            self.currentTool.setLeftClickHandler(shapeClickHandler);
            addToShapesCollection(self.currentTool);
            editMenuCallback();
        });
    }

    this.getCurrentShape = function getCurrentShape(){
        return self.currentTool;
    }

    this.addShapeOnMap = function addShapeOnMap(shape) {
        if(self.isOnMap(shape)){
            return;
        }
        
        shape.getShape().setOptions({ map: self.mapController.map });
        shape.setRightClickHandler(openShapeContextMenu);
        shape.setLeftClickHandler(shapeClickHandler);
        
        addToShapesCollection(shape);

        $.map(shape.children, function(el) { addShapeOnMap(el); })
    }

    this.setShape = function setShape(shape){
        shape.getShape().setOptions({map: self.mapController.map});
        self.currentTool = shape;
        self.currentTool.setRightClickHandler(openShapeContextMenu);
        self.currentTool.setLeftClickHandler(shapeClickHandler);

        addToShapesCollection(shape);
    }

    this.saveCurrentShape = function saveCurrentShape(name, description) {
        if(!self.currentTool) {
            return;
        }
        
        var result;
        if(self.splitLineEditor) {
            self.splitLineEditor.stopEditing();
            self.splitLineEditor = null;
        }
        
        self.currentTool.name = name;
        self.currentTool.description = description;

        self.currentTool.getShape().setOptions({editable: false});
        result = self.currentTool;

        self.currentTool = null;
        return result;
    }

    function editShapeHandler(shape) {
        self.currentTool = shape;        
        shape.enableEditHandlers();
        self.enterShapeEditMode(shape);
        
        if(shape.ctorName == 'MyPlaces.Shapes.Polyline') {
            self.splitLineEditor = new MyPlaces.SplitLineEditor(mapController.map, self.currentTool);
            self.splitLineEditor.startEditing();
        } else {
            shape.getShape().setOptions({editable: true});
        }
    }

    function deleteShapeHandler(shape) {
        
        for(let i = 0; i < shape.children.length; i++) {
            // BUG: This won't work if any child has own children
            myPlaces.storageController.delete(shape.children[i]);
        }

        shape.clear(true);
        
        myPlaces.storageController.delete(shape);
        removeFromShapesCollection(shape);
    }

    function createContextMenuEditButton(shape) {
        let btnEdit = { title: "Edit" };
        btnEdit.handler = ((shape) => { return () => {
            editShapeHandler(shape);
        }})(shape);

        return btnEdit;
    }

    function createContextMenuDeleteButton(shape) {
        let btn = { title: "Delete" };
        btn.handler = ((shape) => { return () => {
            deleteShapeHandler(shape);
        }})(shape);

        return btn;
    }    

    function openShapeContextMenu(latLng, shape) {
        
        let btnEdit = createContextMenuEditButton(shape);
        let btnDelete = createContextMenuDeleteButton(shape);

        self.shapeContextMenuController.openShapeContextMenu(latLng, [ btnEdit, btnDelete ])
    }

    function shapeClickHandler(latLng, shape) {
        if (self.currentTool === null) {
            openInfoView(shape)
        } else {
            self.currentTool.click(latLng);
        }
    }

    function openInfoView(shape) {
        let info = rightPanelMenuController.createViewMenu(shape);
        info.open();
    }

    this.enterShapeEditMode = () => {

    };


    ///////////////

    function mapClick(event) {    
        if(self.currentTool) {
            self.currentTool.click(event.latLng);
        }
    }

    function init() {
        document.addEventListener('mapInitComplete', () => { 
            self.mapController.map.addListener('click', mapClick);

            MyPlaces.Observer.setEvent('mapInitComplete');
        })
    }

    init();

}