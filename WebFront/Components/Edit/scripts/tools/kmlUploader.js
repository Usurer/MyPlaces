MyPlaces.KmlUploader = function(onBeforeUpload, onCompleteUpload, onSuccessCallback) {
    
    let kmlInput = document.getElementById('kmlInput');

    $('#btnUploadKml').click(() => {
        $(kmlInput).trigger('click');
    });

    $(kmlInput).change(() => {
        let files = kmlInput.files;
        if(files.length == 0) {
            return;
        }
        
        onBeforeUpload();

        
        let formData = new FormData();
        formData.append("file", files[0]);

        let url = MyPlaces.Environment.apiUrl + '/api/kml';

        $.ajax(url, {
            method: "POST",
            success: onSuccessCallback,
            complete: onCompleteUpload,
            error: function(){ console.log('AJAX Error'); },
            contentType: false,
            processData: false,
            data: formData
        });
    });
}