if(!MyPlaces) {
    MyPlaces = {};
}

if(!MyPlaces.Toolbox) {
    MyPlaces.Toolbox = function(){};
}

MyPlaces.Toolbox.ButtonsController = function(){

    var buttonsState = {
        tools: { line: 0, polygon: 1, marker: 2, shapeMarker: 3 },
        currentToolId: -1
    };   

    var btnPolyline
        , btnPolygon
        , btnMarker
        , btnShapeMarker
        , btnLoadApi
        , btnKmlUpload
        , buttons;

    this.polylineClickHandler;
    this.polygonClickHandler;
    this.markerClickHandler;
    this.shapeMarkerClickHandler;
    this.setState = setState;

    var self = this;

    function init() {
        btnPolyline = $("#btnPolyline");
        btnPolygon = $("#btnPolygon");
        btnMarker = $("#btnMarker");
        btnShapeMarker = $("#btnShapeMarker");
        btnLoadApi = $("#btnLoadAPI");
        btnKmlUpload = $("#btnUploadKml");

        buttons = [ btnPolyline, btnPolygon, btnMarker, btnShapeMarker, btnLoadApi, btnKmlUpload ];

        btnPolyline.click(function(e) {
            btnClick(buttonsState.tools.line, btnPolyline);
            self.polylineClickHandler();
        });

        btnPolygon.click(function(e) {
            btnClick(buttonsState.tools.polygon, btnPolygon);
            self.polygonClickHandler();
        });

        btnMarker.click(function(e) {
            btnClick(buttonsState.tools.marker, btnMarker);
            self.markerClickHandler();
        });

        btnShapeMarker.click(function(e) {
            btnClick(buttonsState.tools.marker, btnShapeMarker);
            self.shapeMarkerClickHandler();
        });

        btnLoadApi.click(function(e) {
            deactivateAllButtons();
        });

        btnKmlUpload.click(function(e) {
            deactivateAllButtons();
        });
    }

    function btnClick(toolId, el) {
        deactivateAllButtons();
        
        if(buttonsState.currentToolId !== toolId) {
            buttonsState.currentToolId = toolId;
            activateButton(el);
        } else {
            buttonsState.currentToolId = -1; 
            deactivateButton(el);
        }
    }

    function deactivateAllButtons() {
        $.map(buttons, function(e, i){ 
            deactivateButton(e);
        });
    }

    function activateButton(el) {
        el.addClass('selected');
    }

    function deactivateButton(el) {
        el.removeClass('selected');
        el.blur();
    }

    // { polyline: bool, polygon: bool }
    function setState(state) {
        if(state.polyline) {
            btnClick(buttonsState.tools.line, btnPolyline);
        } else if (state.polygon) {
            btnClick(buttonsState.tools.polygon, btnPolygon);
        } else if (state.marker) {
            btnClick(buttonsState.tools.marker, btnMarker);
        } else {
            deactivateAllButtons();
            buttonsState.currentToolId = -1;
        }
    }

    $(document).ready(init);
}