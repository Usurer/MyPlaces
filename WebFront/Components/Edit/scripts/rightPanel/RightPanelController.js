MyPlaces.RightPanelController = function RightPanelController() {
    
    this.open = function openRightBar() {
        var col2 = getCol2();
        var col3 = getCol3();

        clearClasses(col2);
        clearClasses(col3);

        col2.classList.add('animateOpeningDecrease');
        col2.classList.add('flex-10');
        col3.classList.add('animateOpeningIncrease');
        col3.classList.add('flex-5');
    }

    this.close = function closeRightBar() {
        var col2 = getCol2();
        var col3 = getCol3();

        clearClasses(col2);
        clearClasses(col3);
        
        col2.classList.add('animateClosingIncrease');
        col2.classList.add('flex-15');
        col3.classList.add('animateClosingDecrease');
        col3.classList.add('flex-0');
    }

    this.showLoader = function() {
        getCol3().getElementsByClassName('loader-overlay')[0].classList.remove('invisible');
    }

    this.hideLoader = function() {
        getCol3().getElementsByClassName('loader-overlay')[0].classList.add('invisible');
    }

    function getCol2() {
        return document.getElementsByClassName('map-block-col-2')[0];
    }

    function getCol3() {
        return document.getElementsByClassName('map-block-col-3')[0];
    }

    function clearClasses(col) {
        col.classList.remove('animateOpeningDecrease');
        col.classList.remove('animateOpeningIncrease');
        col.classList.remove('animateClosingIncrease');
        col.classList.remove('animateClosingDecrease');
        col.classList.remove('flex-0');
        col.classList.remove('flex-5');
        col.classList.remove('flex-10');
        col.classList.remove('flex-15');
    }
}