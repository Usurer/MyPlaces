MyPlaces.CreateEditMenu = function(shape, wrapperSelector, doneClickHandler, cancelClickHandler) {

    const doneBtnSelector = '.js-done';
    const cancelBtnSelector = '.js-cancel';
    const nameInputSelector = '.js-name';
    const descriptionInputSelector = '.js-description';
    const strokeColorWrapperSelector = '.js-strokeColors-wrapper';
    const fillColorWrapperSelector = '.js-fillColors-wrapper';
    const imagesEditorWrapper = '.js-images-block';

    var menu = this;
    this.shape = shape;

    this.shapeImagesEditor = undefined;
    
    function createColorPickers() {
        // Using arrow-function to prevent wrong context ('this' value) in callbacks
        menu.strokeColorPicker = new MyPlaces.Controls.ColorPicker('Stroke Color', (color) => { menu.shape.setStrokeColor(color) });
        
        if(Object.getOwnPropertyNames(menu.shape.getShape()).find((e) => { return e === 'fillColor';})) {
            menu.fillColorPicker = new MyPlaces.Controls.ColorPicker('Fill Color', (color) => { menu.shape.setFillColor(color) });
        }
    }

    function removeColorPickers() {
        menu.strokeColorPicker.remove();
        if(menu.fillColorPicker) {
            menu.fillColorPicker.remove();
        }
    }

    /**
     * Funcion to execute on Done button click.
     * @param { {name: '', value: ''} } data Input fields values
     */
    const onDoneClick = (data) => { 
        menu.shape.name = data.name;
        menu.shape.description = data.description;
        
        data.removedFiles = menu.shapeImagesEditor.getRemovedFiles();
        data.addedFiles = menu.shapeImagesEditor.getNewFiles();
        
        doneClickHandler(data); 
    };
    
    /**
     * Funcion to execute on Cancel button click.
     */
    const onCancelClick = () => { cancelClickHandler(); };

    this.isOpened = false;
    
    this.wrapperSelector = wrapperSelector;

    this.open = function() {

        if(!menu.initialized) {
            // Wait for TinyMCE init
            setTimeout(menu.open, 100);
            return;
        }

        document.dispatchEvent(new Event('rightPanelMenuBeforeOpen'));

        enableInput(getNameInput());
        //enableInput(getDescriptionInput());
        menu.descriptionEditor.setMode('design');
        enableInput($(menu.wrapperSelector).find(doneBtnSelector));
        enableInput($(menu.wrapperSelector).find(cancelBtnSelector));
        
        createColorPickers();

        let shapeId = '';
        if(menu.shape) {
            getNameInput().val(menu.shape.name);
            getNameInput().siblings('label').addClass('active'); // Fix MDB bug when label won't slide up
            //getDescriptionInput().val(this.shape.description);
            menu.descriptionEditor.setContent(menu.shape.description);
            shapeId = menu.shape.id;
        }

        menu.shapeImagesEditor = new MyPlaces.Components.ShapeImagesEditor($(imagesEditorWrapper), shapeId);
        menu.shapeImagesEditor.load();

        $(menu.wrapperSelector).removeClass('invisible');
        menu.isOpened = true;

        slideIn();
    }

    this.close = function() {
        disableInput(getNameInput());
        //disableInput(getDescriptionInput());
        menu.descriptionEditor.setMode('readonly');

        menu.descriptionEditor.remove() 

        disableInput($(menu.wrapperSelector).find(doneBtnSelector));
        disableInput($(menu.wrapperSelector).find(cancelBtnSelector));

        removeColorPickers();
        
        menu.isOpened = false;
        $(menu.wrapperSelector).addClass('invisible');
        slideOut();        
    }

    this.enterViewMode = function() {
        disableInput(getNameInput());
        // disableInput(getDescriptionInput());
        menu.descriptionEditor.setMode('readonly');

        disableInput($(menu.wrapperSelector).find(doneBtnSelector));
        disableInput($(menu.wrapperSelector).find(cancelBtnSelector));

        removeColorPickers();
    }

    this.enterEditMode = function() {
        enableInput(getNameInput());
        // enableInput(getDescriptionInput());
        menu.descriptionEditor.setMode('design');

        createColorPickers();
    }

    this.updateLength = function(length) {

        document.getElementById('shape_length').innerHTML = Math.floor(length) / 1000 + ' km';
    }

    function done() {
        
        MyPlaces.Observer.subscribe('shape_saveSuccess', () => {
            getNameInput().val('');
            menu.descriptionEditor.setContent('');
            menu.close();    
        })
        
        onDoneClick(getInputValues());
        getNameInput().val('');
    }

    function cancel() {
        getNameInput().val('');
        menu.descriptionEditor.setContent('');
        menu.close();
        onCancelClick(); 
    }

    const init = function() {

        initDescriptionEditor();

        $(menu.wrapperSelector).find(doneBtnSelector).off('click');
        $(menu.wrapperSelector).find(cancelBtnSelector).off('click');
        $(menu.wrapperSelector).find(strokeColorWrapperSelector).children().off('click');
        $(menu.wrapperSelector).find(fillColorWrapperSelector).children().off('click');
        clear();
        
        $(menu.wrapperSelector).find(doneBtnSelector).click(done);
        $(menu.wrapperSelector).find(cancelBtnSelector).click(cancel);
        
        slideOut();
        menu.initialized = true;
    }

    function clear() {
        getNameInput().val('');
        //getDescriptionInput().val('');
        
        menu.descriptionEditor.setContent('');

        menu.done = undefined;
        menu.cancel = undefined;
        $(imagesEditorWrapper).html('');
    }

    function getNameInput() {
        return $(menu.wrapperSelector).find(nameInputSelector).first();
    }

    // function getDescriptionInput() {
    //     return $(menu.wrapperSelector).find(descriptionInputSelector).first();
    // }

    function initDescriptionEditor() {
        tinymce.EditorManager.init({
            selector: '#shape_txtDescription',
            menubar: '',
            menu: '',
            toolbar: 'undo redo | styleselect | bold italic | link',
            init_instance_callback : function(editor) {
                console.log("Editor: " + editor.id + " is now initialized.");
                // editor.setContent('');
                init();
            }
        });

        menu.descriptionEditor = tinymce.activeEditor;
    }
    
    // function getDescriptionInput() {
        
    // }

    function enableInput($el) {
        $el.removeAttr('disabled');
        $el.removeClass('disabled');
    }

    function disableInput($el) {
        $el.attr('disabled', true);
        $el.addClass('disabled');
    }

    function getInputValues() {
        return {
            name: getNameInput().val(),
            description: menu.descriptionEditor.getContent()
        }
    }

    function slideIn(){
        $(menu.wrapperSelector).removeClass('slideOutRight');
        $(menu.wrapperSelector).addClass('slideInRight');
    }

    function slideOut() {
        $(menu.wrapperSelector).removeClass('slideInRight');
        $(menu.wrapperSelector).addClass('slideOutRight');
    }
    
    // init();
    initDescriptionEditor();
}