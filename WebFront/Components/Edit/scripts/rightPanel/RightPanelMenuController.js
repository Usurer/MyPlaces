MyPlaces.RightPanelMenuController = function RightPanelMenuController() {
    
    const self = this;
    
    const menuContainerSelector = '.js-shapeCreateEdit';
    const menus = new Map();
    const panelController = new MyPlaces.RightPanelController();

    this.openEditMenu = function openEditMenu(shape, done, cancel) {
        this.closeMenu();

        let m = createEditMenu(shape, done, cancel);        
        m.open();
    }
    
    function createEditMenu(shape, done, cancel) {
        // This will cause errors if one would like to call createEditMenu for the same shape but with different donde/cancel handlers
        // if(menus.has(shape.id)) {
        //     return menus.get(shape.id);
        // } 

        panelController.open();

        let doneHandler = (data) => {
            panelController.showLoader();
            MyPlaces.Observer.subscribe('shape_saveSuccess', () => { panelController.hideLoader(); panelController.close();}); 
            done(data);  
        };
        let cancelHandler = () => { 
            panelController.close();
            cancel();  
        };

        MyPlaces.Observer.subscribe('shape_changed', (shape) => { self.updateShapeInfo(shape); }, true);

        let m = new MyPlaces.CreateEditMenu(shape, menuContainerSelector, doneHandler, cancelHandler);
        menus.set(shape.id, m);
        return m;
    }

    this.createViewMenu = function createViewMenu(shape) {
        let m = new MyPlaces.ShapeInfoView(shape);
        return m;
    }

    this.updateShapeInfo = function(shape) {
        let length = shape.computeLength();
        menus.get(shape.id).updateLength(length);
    }

    /**
     * Close Shape menu if shape argument is set, close whatever menu is opened right now otherwise
     * This method is to be called if we want menu to be closed without pressing Done or Cancel
     * TODO: What is the usecase for it? 
     * @param {Object} [shape]
     */
    this.closeMenu = function closeMenu(shape) {
        let m = shape
            ? menus.get(shape.id)
            : getOpenedMenu();
        
        if(m) {
            m.close();
        }

        if(shape) {
            menus.delete(shape.id);
        }
        panelController.close();
    }

    function getOpenedMenu() {
        let result;
        for(let m of menus.values()) {
            if(m.isOpened) {
                if(result) {
                    throw "More than 1 opened menu found";
                }
                result = m; 
            }
        }
        return result;
    }

}