MyPlaces.ShapeInfoView = function ShapeInfoView(shape) {

    const wrapperSelector = '.js-infoWindow';
    const nameSelector = '.js-name';
    const descriptionSelector = '.js-description';

    const self = this;
    self.shape = shape;

    this.open = function open() {
        getWrapper().removeClass('invisible');
        getNameElement().html(shape.name);
        getDescriptionElement().html(shape.description);

        document.addEventListener('rightPanelMenuBeforeOpen', this.close);
    }

    this.close = function open() {
        getWrapper().addClass('invisible');
        getNameElement().html('');
        getDescriptionElement().html('');
    }

    function unbindAll() {

    }

    const getWrapper = () => { return $(wrapperSelector).first(); }
    const getNameElement = () => { return $(wrapperSelector + ' ' + nameSelector).first(); }
    const getDescriptionElement = () => { return $(wrapperSelector + ' ' + descriptionSelector).first(); }

}