MyPlaces.Controls = typeof MyPlaces.Controls === 'undefined' ? {} : MyPlaces.Controls;

MyPlaces.Controls.ColorPicker = function ColorPicker(title, onClickCallback) {
    let colors = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet', 'white'];
    let templateSelectorClass = 'js-colorPicker-template';
    let templateSelector = '.' + templateSelectorClass;
    let placeholderSelector = '.js-placeholder';

    let self = this;
    self.element;

    function create() {
        self.element = $(templateSelector).clone();
        self.element.removeClass(templateSelectorClass);
        self.element.find('.js-title').html(title);
        self.element.show();
        $(placeholderSelector).append(self.element);
    }

    this.remove = function remove() {
        self.unbind();
        self.element.remove();
    }

    function setColor(i) {
        self.element.find('.colorPicker__selected').css('background-color', colors[i]);
        onClickCallback(colors[i]);
    }

    function assignClickHandler() {
        self.element.find('.colorPicker__palette').map((i, el) => {
            if (i > 0) {
                $(el).click(((i) => {
                    return () => {
                        setColor(i - 1);
                    }
                })(i));
            }
        });

        self.element.find('.colorPicker__selected').click(
            () => {
                self.element.find('.colorPicker__selected').css('background-color', 'white');
                setColor(7);
            }
        );
    }

    this.unbind = function() {
        self.element.find('.colorPicker__selected').off('click');
        self.element.find('.colorPicker__palette').off('click');
    }

    create()
    assignClickHandler();
}