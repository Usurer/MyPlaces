// use require.js to require all screeps that are needed here
// Require
// - map.js
// - buttons.js
// - journey.js

myPlaces = typeof myPlaces === 'undefined' ? {} : myPlaces;

myPlaces.app = (function app(){
    
    MyPlaces.Observer = new MyPlaces.EventsObserver();

    const map = new MyPlaces.Map();
    const rightPanelMenuController = new MyPlaces.RightPanelMenuController()
    const drawingController = new MyPlaces.Map.DrawingController(map, rightPanelMenuController);

    var tools = { none: 0, line: 1, shape: 2, marker: 3, shapeMarker: 4 } 
    var appState = {
        currentToolId: 0
    }; 

    myPlaces.storageController = new MyPlaces.StorageController();
    

    drawingController.enterShapeEditMode = function(shape) {
        rightPanelMenuController.openEditMenu(shape, shapeEditDone, shapeEditCancel);
        map.map.setOptions({draggableCursor:'crosshair'});
    }

    function subscribeToButtonsController() {

        myPlaces.buttonsController = new MyPlaces.Toolbox.ButtonsController();

        myPlaces.buttonsController.polylineClickHandler = function() {
            if(appState.currentToolId !== tools.line) {
                drawingController.enterPolylinePlotting();
                appState.currentToolId = tools.line;
                rightPanelMenuController.openEditMenu(drawingController.currentTool, shapeEditDone, shapeEditCancel);
                map.map.setOptions({draggableCursor:'crosshair'});
            } else {
                drawingController.cancelCurrentDrawing();
                appState.currentToolId = tools.none;
                rightPanelMenuController.closeMenu();
                map.map.setOptions({draggableCursor:''});
            }
        };
    
        myPlaces.buttonsController.polygonClickHandler = function() {
            if(appState.currentToolId !== tools.shape) {
                drawingController.enterPolygonPlotting();
                appState.currentToolId = tools.shape;
                rightPanelMenuController.openEditMenu(drawingController.currentTool, shapeEditDone, shapeEditCancel);
                map.map.setOptions({draggableCursor:'crosshair'});
            } else {
                drawingController.cancelCurrentDrawing();
                appState.currentToolId = tools.none;
                rightPanelMenuController.closeMenu();
                map.map.setOptions({draggableCursor:''});
            }
        };

        myPlaces.buttonsController.markerClickHandler = function() {
            if(appState.currentToolId !== tools.marker) {
                drawingController.enterMarkerPlotting();
                appState.currentToolId = tools.marker;
                rightPanelMenuController.openEditMenu(drawingController.currentTool, shapeEditDone, shapeEditCancel);
                map.map.setOptions({draggableCursor:'crosshair'});
            } else {
                drawingController.cancelCurrentDrawing();
                appState.currentToolId = tools.none;
                rightPanelMenuController.closeMenu();
                map.map.setOptions({draggableCursor:''});
            }
        };

        myPlaces.buttonsController.shapeMarkerClickHandler = function() {
            if(appState.currentToolId !== tools.shapeMarker) {
                drawingController.enterShapeMarkerPlotting(() => {
                    rightPanelMenuController.openEditMenu(drawingController.currentTool, shapeEditDone, shapeEditCancel)
                });
                appState.currentToolId = tools.shapeMarker;
                map.map.setOptions({draggableCursor:'crosshair'});
            } else {
                $('.notification.shape-marker').addClass('d-none');
                drawingController.cancelCurrentDrawing();
                appState.currentToolId = tools.none;
                rightPanelMenuController.closeMenu();
                map.map.setOptions({draggableCursor:''});
            }
        };
    
        myPlaces.buttonsController.saveClickHandler = function() {
            alert('Not implemented');
        };
    
        myPlaces.buttonsController.loadClickHandler = function() {
            alert('Not implemented');
        };
    
        myPlaces.buttonsController.okClickHandler = function() {
            alert('Not implemented');
        };
    
        myPlaces.buttonsController.cancelClickHandler = function() {
            drawingController.cancelCurrentDrawing();
            appState.currentToolId = tools.none;
        };

    }

    subscribeToButtonsController();

    function shapeEditDone(data) {
        var shape = drawingController.saveCurrentShape(data.name, data.description);

        myPlaces.storageController.putShape(shape, data.removedFiles, data.addedFiles, (s) => {drawingController.addShapeOnMap(s);});        

        appState.currentToolId = tools.none;
        myPlaces.buttonsController.setState({});
        map.map.setOptions({draggableCursor:''});
    }

    function shapeEditCancel() {
        var shapeId = drawingController.getCurrentShape().id;
        drawingController.cancelCurrentDrawing();
        
        var shapeString = myPlaces.storageController.getShape(shapeId);
        
        if(shapeString) {
            var storedShape = MyPlaces.Shapes.utils.deserializeShape(shapeString);
            storedShape.setTimestamp();
            drawingController.setShape(storedShape);
            
            // If we'll try to restore children it may lead to data loss if any child was added after latest shape update in the storage 
            // (child creation doesn't cause stored data refresh; Probably should just fix it)
            // $.map(storedShape.children, function(el) { drawingController.setShape(el); });
        }

        appState.currentToolId = tools.none;
        myPlaces.buttonsController.setState({});
        map.map.setOptions({draggableCursor:''});
    }


    document.addEventListener('mapInitComplete', () => { 
        map.map.addListener('bounds_changed', () => {
            myPlaces.storageController.getAllShapesFromApi(showLoadedShapes, map.getBounds())
        } ); 
    })


    $(document).ready(function(){

        var btn = $('#btnLoadAPI');
        btn.click(function(){
            showLoader();
            myPlaces.storageController.getAllShapesFromApi(showLoadedShapes, map.getBounds());
        });

        MyPlaces.KmlUploader(showLoader, () => {}, showLoadedShapes);
    });

    function showLoadedShapes(data) {
        $.map(data, function(el) { 
            var shape = MyPlaces.Shapes.utils.loadShape(el);
            if(drawingController.isOnMap(shape)) {
                return;
            }
            // I want to store loaded shapes in local storage as well.
            // TODO: There should be a better place for this function
            myPlaces.storageController.storeLocal(shape);
            drawingController.addShapeOnMap(shape);

            // if(shape.ctorName == 'MyPlaces.Shapes.Polyline') {
            //     let calc = new MyPlaces.SplitLineEditor(map.map, shape);
            //     calc.startEditing();
            // }
            
        });
        hideLoader();
    }

    function showLoader() {
        $('.map-block-col-2 .loader-overlay').removeClass('invisible');
    }

    function hideLoader() {
        $('.map-block-col-2 .loader-overlay').addClass('invisible');
    }

    return {
        appState: appState,
        initMap: map.initMap
    }
})();