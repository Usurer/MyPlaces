if (typeof MyPlaces === 'undefined') {
    MyPlaces = {};
}

if(typeof MyPlaces.Components === 'undefined') {
    MyPlaces.Components = {};
}

MyPlaces.Components.CreateEditGallery = function(el, imagesApiUrl) {
    
    const self = this;
    const htmlPath = '/components/CreateEditGallery/index.html';
    const mainSelector = '[mp-createEditGallery]';
    const removed = [];
    const element = el;
    const imgWidth = 225;
    const imgHeight = 141;

    this.load = function load() {
        $.get(htmlPath).done((html) => {
            $(element).append(html);
            self.getImages(imagesApiUrl);
        });
    }

    /**
     * An API call. Should return an array of { id, name, src } objects
     * @param {String} url 
     */
    this.getImages = function (url) {
        $.get(url)
            .done((data) => {
                $(mainSelector).html('');

                for(let i = 0; i < data.length; i++) {
                    let img = document.createElement('img');
                    img.src = data[i].src + '/width/' + imgWidth +  '/height/' + imgHeight;
                    img.alt = data[i].name;

                    let wrapper = document.createElement('div');
                    wrapper.className = 'img_wrapper';

                    let overlay = document.createElement('div');
                    overlay.className = 'img_overlay';

                    let iconDel = document.createElement('div');
                    iconDel.className = 'icon_del';
                    iconDel.addEventListener('click', (e) => { toggleRemoval(e.target, data[i].name); });

                    $(overlay).append(iconDel);

                    $(wrapper).append(overlay);
                    $(wrapper).append(img);
                    $(mainSelector).append(wrapper);
                }
            });
    }

    this.getRemovedNames = function() {
        return removed;
    }

    let toggleRemoval = function(el, name) {
        if(!removed.includes(name)) {
            el.className = 'icon_undo';
            $(el).parent().addClass('opaque');
            removed.push(name);
        } else {
            el.className = 'icon_del';
            $(el).parent().removeClass('opaque');
           
            let i = removed.indexOf(name);
            removed.splice(i, 1);
        }
    }
}