if (typeof MyPlaces === 'undefined') {
    MyPlaces = {};
}

if(typeof MyPlaces.Components === 'undefined') {
    MyPlaces.Components = {};
}

MyPlaces.Components.ShapeImagesEditor = function(el, shapeId) {
    
    const self = this;
    const htmlPath = '/components/ShapeImagesEditor/index.html';
    const uploaderSelector = '.js-uploader';
    const gallerySelector = '.js-gallery-wrapper';
    const element = el;

    let galleryController;
    let dropzone;

    this.load = function load() {
        return $.get(htmlPath).done((html) => {
            $(element).html('');
            
            $(element).append(html);
            self.createDropzone();

            if(shapeId) {
                galleryController = new MyPlaces.Components.CreateEditGallery($(gallerySelector), MyPlaces.Environment.apiUrl + '/api/images/shape/' + shapeId);
                galleryController.load();
            }
        });
    }

    this.createDropzone = function() {
        Dropzone.autoDiscover = false;
        dropzone = new Dropzone(uploaderSelector, {
            autoProcessQueue: false,
            paramName: "files",
            url: '/'
        });
    }

    this.getNewFiles = function() {
        return dropzone.files;
    }

    this.getRemovedFiles = function() {
        return galleryController ? galleryController.getRemovedNames() : [];
    }

}