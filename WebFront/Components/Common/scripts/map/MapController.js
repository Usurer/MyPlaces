MyPlaces.Map = function MyPlacesMap() {
    
    // TODO: Implement polygon plotting like it's done in drawing tools for GMaps (https://developers.google.com/maps/documentation/javascript/examples/drawing-tools)
    
    const self = this
    this.map = null;
    
    var event = new Event('mapInitComplete');

    const antalya = { lat: 36.316, lng: 29.952 };
    const stPetersburg = { lat: 60.488, lng: 30.250 };

    this.initMap = function initMap() {
        self.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 9,
            center: stPetersburg 
        });

        document.dispatchEvent(event);
    }

    this.mapResize =  function mapResize() {
        google.maps.event.trigger(self.map, 'resize');
    }

    this.getBounds = function() {
        let bounds = self.map.getBounds();
        return {
            top: bounds.getNorthEast().lat(),
            right: bounds.getNorthEast().lng(),
            bottom: bounds.getSouthWest().lat(),
            left: bounds.getSouthWest().lng(),
        };
    }
}