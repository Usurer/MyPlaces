MyPlaces.SplitLineEditor = function(map, shape) {
    const self = this;
    self.nearestPointIndex;
    self.latestFixedIndex = -1;
    self.wasSplit = false;
    self.moveListener = undefined;

    let shape1, shape2, editableShape;

    shape1 = new google.maps.Polyline({
        strokeColor: shape.shapeObject.strokeColor,
        strokeOpacity: shape.shapeObject.strokeOpacity,
        strokeWeight: shape.shapeObject.strokeWeight,
        map: map
    });

    shape2 = new google.maps.Polyline({
        strokeColor: shape.shapeObject.strokeColor,
        strokeOpacity: shape.shapeObject.strokeOpacity,
        strokeWeight: shape.shapeObject.strokeWeight,
        map: map
    });

    editableShape = new google.maps.Polyline({
        strokeColor: shape.shapeObject.strokeColor,
        strokeOpacity: shape.shapeObject.strokeOpacity,
        strokeWeight: shape.shapeObject.strokeWeight,
        map: map,
        editable: true
    });

    this.startEditing = function() {
        self.moveListener = map.addListener('mousemove', (e) => {    
            let closestPointInfo = getNearestPoint(e.latLng)
            updateLine(closestPointInfo.latLng, closestPointInfo.index);
        });
    }

    this.stopEditing = function() {
        google.maps.event.removeListener(self.moveListener);

        self.wasSplit = false;
        shape1.setOptions( { map: null, path: [] } );
        shape2.setOptions( { map: null, path: [] } );
        editableShape.setOptions( { map: null, path: [] } );
        shape.shapeObject.setMap(map);
    }

    function updateLine(newNearestPoint, index) {
        if(self.latestFixedIndex == -1) {
            self.latestFixedIndex = index;
        }

        if(shape.getPath().getArray().length < 15) {
            saveShape();
            splitShape();
        }

        if(self.nearestPoint != newNearestPoint) {
            
            let delta = index - self.latestFixedIndex;
            let sign = delta >= 0 ? 1 : -1;
            let diff = Math.abs(delta) - 4;

            if(diff > 0) {
                
                saveShape();

                let newCenter = shape.getPath().getArray()[self.latestFixedIndex + diff * sign];
                self.latestFixedIndex = self.latestFixedIndex + diff * sign;
                if (self.latestFixedIndex > shape.getPath().getArray().length - 1) {
                    self.latestFixedIndex = shape.getPath().getArray().length - 1;
                } if (self.latestFixedIndex < 0) {
                    self.latestFixedIndex = 0;
                }
                self.nearestPoint = newCenter;
                
                splitShape();
            }
            
        }
    }

    function getNearestPoint(latLng) {
        let path = shape.shapeObject.getPath();
        
        let min = 20000 * 1000; // roughly 0.5 of equator length in meters
        let nearestPoint;

        let i = 0;
        path.forEach(point => {
            
            let distance = google.maps.geometry.spherical.computeDistanceBetween(point, latLng);
            if(distance < min) {
                min = distance;                
                nearestPoint = point;
                self.nearestPointIndex = i;
            }

            i = i + 1;
        });

        return {
            latLng : nearestPoint,
            index: self.nearestPointIndex
        };
    }

    function saveShape() {
        if(self.wasSplit) {
            let a = shape1.getPath().getArray().slice(0, shape1.getPath().getArray().length - 1); // skip last point that was added to prevent gaps
            let b = editableShape.getPath().getArray();
            let c = shape2.getPath().getArray().slice(1, shape2.getPath().getArray().length); // skip first point that was added to prevent gaps
            let newPath = a.concat(b.concat(c));
            
            shape.shapeObject.setOptions({ path: newPath });
        }
    }

    function splitShape() {
        let index = self.latestFixedIndex;
        let path = shape.shapeObject.getPath().getArray();
        let legSize = 6;
        
        let indexStart = index - legSize;
        if (indexStart < 0) {
            indexStart = 0;
        }

        let indexEnd = index + legSize;
        if (indexEnd >= path.length) {
            indexEnd = path.length - 1;
        }

        if (indexEnd - indexStart < legSize) {
            var firstPart = [];
            var lastPart = [];
            var middle = path;    
        } else {
            firstPart = path.slice(0, indexStart + 1 ); // First and Last should contain points from Editable shape to prevent gaps
            lastPart = path.slice(indexEnd, path.length);
            middle = path.slice(indexStart, indexEnd + 1);
        }

        shape1.setOptions({path : firstPart});
        shape2.setOptions({path : lastPart});
        editableShape.setOptions({path : middle});

        shape.shapeObject.setMap(null);
        self.wasSplit = true;
    }
}