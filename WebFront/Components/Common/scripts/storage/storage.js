if(!MyPlaces) {
    MyPlaces = {};
}

MyPlaces.StorageController = function() {
    
    const self = this;
    this.storage = sessionStorage;

    this.keys = [];

    this.storeLocal = function(shape) {
        var str = shape.serialize();
        this.storage.setItem(shape.id, str);
        this.keys.push(shape.id);
    }

    this.post = function(shape, removedFiles, addedFiles, onSuccess) {
        var str = shape.serialize();

        let formData = new FormData();

        if(addedFiles) {
            for (let i = 0; i < addedFiles.length; i++) {
                let file = addedFiles[i];
                formData.append("newFiles", file);
            }
        }

        formData.append('shape', str);

        if(removedFiles) {
            formData.append('removedFilesNames', JSON.stringify(removedFiles));
        }

        let url = MyPlaces.Environment.apiUrl + '/api/shapes';

        $.ajax(url, {
            method: "POST",
            success: (function(shape) {               
                
                return function(shapeId){ 
                    shape.setTimestamp(); 
                    shape.id = shapeId;
                    self.storeLocal(shape);
                    if(onSuccess) {
                        onSuccess(shape);
                    }
                }
            })(shape),
            error: function(){ console.log('AJAX Error'); },
            complete: () => MyPlaces.Observer.setEvent('shape_saveSuccess'),
            contentType: false,
            processData: false,
            data: formData
        }); 
    }

    this.putShape = function(shape, removedFiles, addedFiles, onSuccess) {
        this.post(shape, removedFiles, addedFiles, onSuccess);
        return;
    }

    this.delete = function(shape) {
        let url = MyPlaces.Environment.apiUrl + '/api/shapes/';

        $.ajax(url + shape.id, {
            method: "DELETE",
            success: () => {},
            error: function(){ console.log('AJAX Error'); },
            contentType: "application/json"
        });
    }

    /**
     * Returns Shape from local storage; synchronous function.
     * @returns {String} Serialized IShape object
     */
    this.getShape = function(id) {
        var str = this.storage.getItem(id);
        if(typeof str === 'undefined' || str == null) {
            return undefined;
        }

        return str;
    }

    this.getAllShapesFromApi = function(showOnMap, bounds) {
        let url = MyPlaces.Environment.apiUrl + '/api/shapes';
        if(typeof bounds !== 'undefined') {
            url = url + "/" + bounds.top + "/" + bounds.bottom + "/" + bounds.left + "/" + bounds.right;
        }

        $.ajax(url, {
            method: "GET",
            success: showOnMap,
            error: function(){ console.log('AJAX Error'); }
        });
    }

    this.getString = function(id) {
        return this.storage.getItem(id);
    }

    this.removeShape = function(id) {
        this.storage.remove(id);
    }

    this.getDetails = function(id, onSuccess) {
        let url = MyPlaces.Environment.apiUrl + '/api/shapes/details/' + id;
        $.ajax(url, {
            method: "GET",
            success: (data) => onSuccess(data),
            error: function(){ console.log('AJAX Error'); }
        });
    }

}