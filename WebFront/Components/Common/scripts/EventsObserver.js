MyPlaces.EventsObserver = function() {

    const subscribers = new Map();

    this.subscribe = function(eventType, callback, doNotUnsubscribe) {
        let eventSubscribers = subscribers.get(eventType);
        if (typeof eventSubscribers === 'undefined') {
            eventSubscribers = [];
        }

        eventSubscribers.push([callback, doNotUnsubscribe]);
        subscribers.set(eventType, eventSubscribers);
    }

    this.setEvent = function(eventType, eventData) {
        let eventSubscribers = subscribers.get(eventType);
        if (typeof eventSubscribers === 'undefined') {
            return;
        }

        eventSubscribers.forEach((x) => {
            x[0](eventData);
        })

        eventSubscribers = eventSubscribers.filter(x => x[1]);
        subscribers.set(eventType, eventSubscribers);       
    }

}