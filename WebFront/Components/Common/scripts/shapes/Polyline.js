

MyPlaces.Shapes.Polyline = function (map) {
    
    this.ctorName = 'MyPlaces.Shapes.Polyline';

    this.shapeObject = new google.maps.Polyline({
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        editable: true,
        map: map
    });

    this.length = 0;

    this.mouseover = (function(shape) {
        return function() { shape.setOptions({ strokeWeight: 7 }) }
    })(this.shapeObject);

    this.mouseout = (function(shape) {
        return function() { shape.setOptions({ strokeWeight: 3 }) }
    })(this.shapeObject);

    this.edit_setAt = () => {
        MyPlaces.Observer.setEvent('shape_changed', this);
    }

    this.edit_insertAt = () => {
        MyPlaces.Observer.setEvent('shape_changed', this);
    }

    this.edit_removeAt = () => {
        MyPlaces.Observer.setEvent('shape_changed', this);
    }

    this.click = function(latLng) {
        this.shapeObject.getPath().push(latLng);
    }

    this.computeLength = function() {
        return google.maps.geometry.spherical.computeLength(this.shapeObject.getPath());
    }
}

MyPlaces.Shapes.Polyline.prototype = new MyPlaces.Shapes.IShape();