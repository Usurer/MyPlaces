MyPlaces.Shapes.Marker = function(map) {
    
    this.ctorName = 'MyPlaces.Shapes.Marker';

    this.shapeObject = new google.maps.Marker({
        map: map
    });

    this.click = function(latLng) {
        this.shapeObject.setOptions({ position: latLng });
    }

    this.serialize = function() {
        return JSON.stringify({
            ctorName: this.ctorName,
            name: this.name,
            description: this.description,
            id: this.id,
            position: this.getPostition().toJSON(),
            parentId: typeof this.parent !== 'undefined' ? this.parent.id : null
        });
    }

    this.fromObject = function(obj) {
        this.id = obj.id;
        this.name = obj.name;
        this.description = obj.description;
        this.shapeObject.setOptions({ position: obj.position, editable: false});
        return this;
    } 

    this.getPath = function() {
        return null;
    }

    this.getPostition = function() {
        return this.shapeObject.position;
    }

    this.enableEditHandlers = function() {
    }
}

MyPlaces.Shapes.Marker.prototype = new MyPlaces.Shapes.IShape();    