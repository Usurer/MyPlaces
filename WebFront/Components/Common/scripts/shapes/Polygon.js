
MyPlaces.Shapes.Polygon = function (map) {
    
    this.ctorName = 'MyPlaces.Shapes.Polygon';
    
    this.shapeObject = new google.maps.Polygon({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        editable: true, 
        map: map
    });

    this.mouseover = function(){};
    this.mouseout = function(){};

    this.click = function(latLng) {
        this.shapeObject.setPath(MyPlaces.Shapes.utils.createDummyPolygonPath(this.shapeObject.getMap(), latLng));
    }
}

MyPlaces.Shapes.Polygon.prototype = new MyPlaces.Shapes.IShape();