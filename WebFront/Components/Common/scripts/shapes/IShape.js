if (typeof MyPlaces == 'undefined') {
    MyPlaces = {};
}

if (typeof MyPlaces.Shapes == 'undefined') {
    MyPlaces.Shapes = {};
}

MyPlaces.Shapes.IShape = function() {
    this.name = '';
    this.description = '';
    this.shapeObject = null; // Polyline, Polygon, Marker
    this.id = ''; // Guid
    this.ctorName = 'IShape';
    this.timestamp = null; // updated on successful Create/Update POST; should be set on Shape load from API
    this.color = 'black';
    this.fillColor = '#FF0000';

    this.children = []; // array of IShape - main purpose is a Markers storage
    this.parent; // link to a parent if a shape is a child

    this.mouseover = function(){ console.log('Not implemented mouseover handler call.')};
    this.mouseout = function(){ console.log('Not implemented mouseout handler call.')};

    this.edit_setAt = function(){ console.log('Not implemented set_at handler call.')};
    this.edit_removeAt = function(){ console.log('Not implemented remove_at handler call.')};
    this.edit_insertAt = function(){ console.log('Not implemented insert_at handler call.')};

    this.computeLength = () => { console.log('Not implemented computeLength call.'); }

    this.init = function() {
        this.shapeObject.addListener('mouseover', this.mouseover);
        this.shapeObject.addListener('mouseout', this.mouseout);
    }

    this.enableEditHandlers = function() {
        google.maps.event.addListener(this.shapeObject.getPath(), 'set_at', this.edit_setAt);
        google.maps.event.addListener(this.shapeObject.getPath(), 'remove_at', this.edit_removeAt);
        google.maps.event.addListener(this.shapeObject.getPath(), 'insert_at', this.edit_insertAt);
    }

    this.getShape = function() {
        return this.shapeObject;
    }

    /**
     * Clears shape path and removes the shape from it's parent Children collection
     * @param {Boolean} [clearChildren] If true will call clear() on shape children as well
     */
    this.clear = function(clearChildren = false) {
        // Marker has no getPath method
        if(this.shapeObject.getPath) {
            this.shapeObject.getPath().clear();
        }
        
        this.shapeObject.setMap(null);
        
        if(clearChildren === true) {
            for(var i = 0; i < this.children.length; i++) {
                if(this.children[i]) {
                    this.children[i].clear(clearChildren);
                }
            }
            this.children = [];
        }

        if(this.parent){
            this.parent.removeChildById(this.id);
        }
        
    }

    

    this.getShapeTypeByCtorName = function(shape) {
        return shape.ctorName == 'MyPlaces.Shapes.Polyline'
            ? 'Line'
            : shape.ctorName == 'MyPlaces.Shapes.Polygon'
                ? 'Polygon'
                : 'Marker';
    }

    this.click = function(latLng) {
        throw 'Click is not implemented in base class';
    }

    this.setRightClickHandler = function(handler) {
        this.shapeObject.addListener('rightclick', (function(shape) {
            return function(e) { 
                return handler(e.latLng, shape);
            }
        })(this));
    }

    this.setLeftClickHandler = function(handler) {
        this.shapeObject.addListener('click', (function(shape) {
            return function(e) {
                MyPlaces.Observer.setEvent('shapeClick', shape);
                return handler(e.latLng, shape);
            }
        })(this));
    }

    this.addChild = function(shape) {
        this.children.push(shape);
        shape.parent = this;
    }

    this.removeChildById = function(id) {
        for(var i = 0; i < this.children.length; i++) {
            if(i.id === id)
                this.children.splice(i, 1);
        }
    }

    this.getPath = function() {
        return this.shapeObject.getPath();
    }

    this.simplify = function(shape) {
        return {
            //ctorName: shape.ctorName,
            shapeType: shape.getShapeTypeByCtorName(shape),
            name: shape.name,
            description: shape.description,
            strokeColor: shape.strokeColor,
            fillColor: shape.fillColor,
            id: shape.id,
            path: (function(shape) {
                var path = shape.getPath();
                if(path) {
                    return $.map(path.getArray(), function(el) { return el.toJSON(); } )
                } else {
                    return null;
                }
            })(shape),
            children: (function(parent) {
                if(parent.children && parent.children.length > 0) {
                    var result =  $.map(parent.children, function(el){ return el.simplify(el); });
                    return result;
                }
                return [];
            })(shape),
            position: (function(shape){
                var val = typeof shape.getPostition !== 'undefined' ? shape.getPostition().toJSON() : null;
                return val;
            })(shape)
        }
    }

    this.serialize = function() {
        return JSON.stringify(this.simplify(this));
    }

    this.fromObject = function(obj) {
        this.id = obj.id;
        this.name = obj.name;
        this.description = obj.description;
        this.strokeColor = obj.strokeColor;
        this.fillColor = obj.fillColor;

        // TODO: It was added to handle API response, but I want server to return parsed data
        var parsedPath = $.map(obj.path, function(el) {
            if (typeof el.lat != 'number') {
                return {
                    lat: parseFloat(el.lat),
                    lng: parseFloat(el.lng)
                }
            } else {
                return el;
            }
        })

        this.shapeObject.setPath(parsedPath);
        this.shapeObject.setOptions({editable: false, strokeColor: this.strokeColor, fillColor: this.fillColor});

        this.children = $.map(obj.children, 
            (function (parent) {
                return function(el){ 
                    var child = MyPlaces.Shapes.utils.loadShape(el);
                    child.parent = parent;
                    return child;
                }
            })(this)
        );

        return this;
    }

    // Timestamp is used to block or allow context menu. Though the idea seems ok, it may lead to bugs baceuse it's not very clear how is timestamp related to context menu.
    // Maybe it would be better to have function like allowContextMenu - but that, from another PoV, means kinda lot of micromanagement.
    this.setTimestamp = function() {
        this.timestamp = Date.now();
        $.map(this.children, function(el) { el.setTimestamp(); });
    }

    this.setStrokeColor = function(color) {
        this.strokeColor = color;
        this.shapeObject.setOptions( { strokeColor : this.strokeColor });
    }

    this.setFillColor = function(color) {
        this.fillColor = color;
        this.shapeObject.setOptions( { fillColor : this.fillColor });
    }
}