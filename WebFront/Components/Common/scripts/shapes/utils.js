if (typeof MyPlaces == 'undefined') {
    MyPlaces = {};
}

if (typeof MyPlaces.Shapes == 'undefined') {
    MyPlaces.Shapes = {};
}

MyPlaces.Shapes.utils = (function() {

    var constructors = {
        'MyPlaces.Shapes.Polygon': MyPlaces.Shapes.Polygon,
        'MyPlaces.Shapes.Polyline': MyPlaces.Shapes.Polyline,
        'MyPlaces.Shapes.Marker': MyPlaces.Shapes.Marker
    };

    function createNewShape(shapeCtor, map) {
        var shape = new shapeCtor(map);
        shape.init();
        return shape;
    }

    function createNewPolyline(map){
        return createNewShape(MyPlaces.Shapes.Polyline, map);
    }

    function createNewPolygon(map){
        return createNewShape(MyPlaces.Shapes.Polygon, map);
    }

    function createNewMarker(map){
        return createNewShape(MyPlaces.Shapes.Marker, map);
    }

    function createDummyPolygonPath(map, latLng){
        var bounds = map.getBounds();

        var southWest = bounds.getSouthWest();
        var northEast = bounds.getNorthEast();

        var latBound = northEast.lat() - southWest.lat();
        latBound = latBound > 0 ? latBound : -latBound;
        var lngBound = northEast.lng() - southWest.lng();
        lngBound = lngBound > 0 ? lngBound : -lngBound;

        var latModifier = latBound / 10;
        var lngModifier = lngBound / 30;
        
        return [
            {lat: latLng.lat(), lng: latLng.lng()},
            {lat: latLng.lat() + latModifier, lng: latLng.lng()},
            {lat: latLng.lat() + latModifier, lng: latLng.lng() + lngModifier},
            {lat: latLng.lat(), lng: latLng.lng() + lngModifier}];
    }

    // WARN: deserialize and load are very similar and it's not clear when should I use one or another. Please do smthg about it.
    function deserializeShape(str) {
        let plainObject = JSON.parse(str);
        let ctorName = getCtorByShapeType(plainObject.shapeType);
        let shape = createNewShape(constructors[ctorName]);
        return shape.fromObject(plainObject);
    }

    function loadShape(obj) {
        let ctorName = getCtorByShapeType(obj.shapeType);
        var shape = createNewShape(constructors[ctorName]);
        shape.setTimestamp();
        return shape.fromObject(obj);
    }

    function getCtorByShapeType(shapeTypeString) {
        return shapeTypeString == 'Line'
            ? 'MyPlaces.Shapes.Polyline'
            : shapeTypeString == 'Polygon'
                ? 'MyPlaces.Shapes.Polygon'
                : 'MyPlaces.Shapes.Marker';
    }

    return {
        createNewPolyline: createNewPolyline,
        createNewPolygon: createNewPolygon,
        createDummyPolygonPath: createDummyPolygonPath,
        createNewMarker: createNewMarker,
        deserializeShape: deserializeShape,
        loadShape: loadShape
    };
})();