myPlaces = (() => {
    
    const self = this;
    
    MyPlaces.Observer = new MyPlaces.EventsObserver();

    this.mapController = new MyPlaces.Map();
    this.storageController = new MyPlaces.StorageController();
    this.shapes = new Map();
    
    function initMap() {
        document.getElementById('mapLoader').classList.remove('invisible');

        self.mapController.initMap();
        
        /*  After Maps API is initialized, it still takes some time to load and draw the map tiles. It is possible to plot shapes
            before the map tiles are drawn, but I don't like it - a gray background with lines over it. BUT! It will make the app faster.
            Instead of that - since the speed is not my main concern now - I want to load and plot shapes only after the Map is fully loaded.
            BTW it may be a good idea to load shapes from API before map tiles are drawn, and plot these shapes after.
            So I'm using the 'idle' event to check if Map is, well, idle, meaning that all the map data was loaded and shown. Although there are reports
            that it's not the reliable check and won't work everywhere.
        */
        google.maps.event.addListenerOnce(self.mapController.map, 'idle', function(){
            loadShapes();
            
            document.getElementById('mapLoader').classList.add('invisible');
            
            self.mapController.map.addListener('bounds_changed', () => {
                loadShapes();
            } ); 
        });

        $('#modal').on('shown.bs.modal', function () {
            let shapeId = $('#modal').data('shapeId');
            let galleryElement = document.getElementById('shape-details__gallery');
            showShapeGallery(shapeId, galleryElement, false);

            $('.shape-details__child').toArray().forEach(x => {
                let shapeId = $(x).data('shapeId');
                let galleryElement = $(x).find('.shapeDetails__child__gallery')[0];
                showShapeGallery(shapeId, galleryElement, true);
            });
        });
    }

    function loadShapes() {
        self.storageController.getAllShapesFromApi(onShapesLoaded, self.mapController.getBounds())
    }

    function onShapesLoaded(arr) {
        let addShapeWithChildren = (shape) => {
            if (self.shapes.has(shape.id)) {
                return;
            } else {
                self.shapes.set(shape.id, shape);
            }

            shape.getShape().setOptions({ map: self.mapController.map });
            shape.setLeftClickHandler(onShapeClick);
            
            shape.children.forEach(child => {
                addShapeWithChildren(child);
            });
        };
        
        arr.forEach(x => {
            let shape = MyPlaces.Shapes.utils.loadShape(x);
            addShapeWithChildren(shape);
        });
        
    }

    // Moved from MapController //////////////////
    
    function onShapeClick(latLng, shape) {
        document.getElementById('shape-details__title').innerHTML = shape.name;
        document.getElementById('shape-details__description').innerHTML = shape.description;
        document.getElementById('js-shape-details__children').innerHTML = '';
        
        showMainImage(shape);

        $('#modal').data('shapeId', shape.id);
        $('#modal').modal('show');
        
        showChildrenDetails(shape);

        self.storageController.getDetails(shape.id, (data) => {
            if (data != null)
                document.getElementById('shape-details__params').innerHTML = data + ' km';
        })
    }

    function clearGallery() {
        document.getElementById('shape-details__gallery').innerHTML = '';
    }

    function showShapeGallery(shapeId, galleryElement, isChildGallery) {
        
        galleryElement.innerHTML = '';
        
        // TODO: this should be done on Modal close
        $(galleryElement).removeData('flexslider');
        $(galleryElement).addClass('d-none');

        let ul = document.createElement('ul');
        ul.classList.add('slides');

        let url = MyPlaces.Environment.apiUrl + '/api/images/shape/' + shapeId;
        $.get(url)
            .done((data) => {
                if(data.length <= 1 && !isChildGallery) {
                    // If there's only one image, it will be shown as a main Shape image, thus no gallery is needed
                    // Children don't have main image when shown in parent details, so gallery should be visible
                    return;
                }
                data.forEach(x => {
                    let img = document.createElement('img');
                    img.src = x.src + '/width/' + 231 +  '/height/' + 129;
                    img.alt = x.name;

                    let li = document.createElement('li');
                    li.appendChild(img);
                    ul.appendChild(li);
                });

                galleryElement.appendChild(ul);
                $(galleryElement).removeClass('d-none');

                $('.flexslider').flexslider({
                    animation: "slide",
                    animationLoop: false,
                    itemWidth: 231,
                    itemMargin: 5
                });
            });
    }

    function showChildrenDetails(shape) {
        //let children = self.shapes.filter(x => shape.children.find(y => y.id === x.id));
        let children = shape.children;
        let template = $('.js-shapeDetailsChildrenTemplate').first();        
        
        children.forEach(x => {
            let wrapper = template.clone();
            // TODO: class will be removed for every clone - maybe it would be better to optimize it something - like use ID or attribute to find a template
            wrapper.removeClass('js-shapeDetailsChildrenTemplate');

            wrapper.find('.js-childTitle').html(x.name);
            wrapper.find('.js-childDescription').html(x.description);
            
            $('#js-shape-details__children').append(wrapper);
            wrapper.css('display', 'block');

            wrapper.data('shapeId', x.id);
        });
    }

    function showMainImage(shape) {
        let img = document.getElementById('shape-details__image');
        
        img.src = '';
        img.classList.add('d-none');

        let url = MyPlaces.Environment.apiUrl + '/api/images/shape/' + shape.id;
        $.get(url)
            .done((data) => {
                if(data.length == 0) return;
                
                let mainImage = data[0];                
                img.src = mainImage.src + '/width/' + 400 + '/height/' + 225;
                img.classList.remove('d-none');
            });
    }
    
    // This was in initMap function
    $('#modal').on('hidden.bs.modal', clearGallery);

    ////////////////////////////////////

    return {
        initMap: initMap
    };
    
})();