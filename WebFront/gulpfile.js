'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var del = require('del');

gulp.task('buildBootstrap:copySrc', () => {
    return gulp.src('./node_modules/bootstrap/scss/**/*.scss')        
        .pipe(gulp.dest('./temp_build/scss'))
});

gulp.task('buildBootstrap:merge', ['buildBootstrap:copySrc'], () => {
    return gulp.src('./Components/Common/styles/sass/_custom.scss')        
        .pipe(gulp.dest('./temp_build/scss'))
});

gulp.task('buildBootstrap', ['buildBootstrap:merge'], () => {
    return Promise.all([
        new Promise((resolve, reject) => {
            gulp.src('./temp_build/scss/**/*.scss')
                .pipe(sass().on('error', sass.logError))
                .pipe(sourcemaps.init())
                .pipe(postcss([ autoprefixer() ]))
                .pipe(sourcemaps.write('.'))
                .pipe(gulp.dest('./Components/Common/vendor/Bootstrap/css'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            gulp.src('./node_modules/jquery/dist/jquery.min.js')
                .pipe(gulp.dest('./Components/Common/vendor/JQuery'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            gulp.src('./node_modules/bootstrap/dist/js/*.js')
                .pipe(gulp.dest('./Components/Common/vendor/Bootstrap/js'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            gulp.src('./node_modules/tether/dist/js/tether.js')
                .pipe(gulp.dest('./Components/Common/vendor/Bootstrap/js'))
                .on('end', resolve);
        })
    ]).then(() => del(['./temp_build']));
})

gulp.task('editor:buildVendor', ['buildBootstrap', 'buildBootstrap:merge', 'buildBootstrap:copySrc'], () =>{
    return Promise.all([
        new Promise((resolve, reject) => {
            gulp.src('./node_modules/dropzone/dist/**')
                .pipe(gulp.dest('./Components/Common/vendor/dropzone'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            gulp.src('./node_modules/tinymce/*.min.js')
                .pipe(gulp.dest('./Components/Common/vendor/tinymce'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            gulp.src('./node_modules/tinymce/skins/**/*')
                .pipe(gulp.dest('./Components/Common/vendor/tinymce/skins'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            gulp.src('./node_modules/tinymce/themes/**/*')
                .pipe(gulp.dest('./Components/Common/vendor/tinymce/themes'))
                .on('end', resolve);    
        })
    ]);
});

gulp.task('common:vendor', [], () =>{
    return Promise.all([
        new Promise((resolve, reject) => {
            gulp.src(['./node_modules/flexslider/jquery.flexslider.js'
                , './node_modules/flexslider/flexslider.css'])
                .pipe(gulp.dest('./Components/Common/vendor/flexslider'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            gulp.src(['./node_modules/flexslider/fonts/**/*'])
                .pipe(gulp.dest('./Components/Common/vendor/flexslider/fonts'))
                .on('end', resolve);
        })
    ]);
});

gulp.task('sass:common', () => {
    return gulp.src('./Components/Common/styles/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest('./Components/Common/styles/'));
});

gulp.task('sass:editor', () => {
    return gulp.src('./Components/Edit/styles/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest('./Components/Edit/styles/'));
});

gulp.task('sass:viewer', () => {
    return gulp.src('./Components/View/styles/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest('./Components/View/styles/'));
});

gulp.task('copy:editor', ['sass:editor', 'sass:common', 'common:vendor', 'editor:buildVendor'], () => {
    /*  Figure brackets syntax allows me to exclude folder and it's contents. See https://github.com/gulpjs/gulp/issues/165 
        Important - there should be no spaces between patterns inside square brackets, i.e. {a,b}, not {a, b}*/
    
    return Promise.all([
        new Promise((resolve, reject) => {
            return gulp.src(['./Components/Common/**/*', '!./Components/Common/styles/{sass,sass/**}'])
                .pipe(gulp.dest('./Edit'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            return gulp.src(['./Components/Edit/**/*', '!./Components/Edit/styles/{sass,sass/**}'])
                .pipe(gulp.dest('./Edit'))
                .on('end', resolve);
        })
    ]);
})

gulp.task('copy:viewer', ['sass:viewer', 'sass:common', 'common:vendor', 'buildBootstrap'], () => {
    /*  Figure brackets syntax allows me to exclude folder and it's contents. See https://github.com/gulpjs/gulp/issues/165 
        Important - there should be no spaces between patterns inside square brackets, i.e. {a,b}, not {a, b}*/
    
    return Promise.all([
        new Promise((resolve, reject) => {
            gulp.src(['./Components/Common/**/*', '!./Components/Common/styles/{sass,sass/**}'])
                .pipe(gulp.dest('./View'))
                .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            gulp.src(['./Components/View/**/*', '!./Components/View/styles/{sass,sass/**}'])
                .pipe(gulp.dest('./View'))
                .on('end', resolve);
        })
    ]);
})


gulp.task('build', ['build-editor', 'build-viewer'], function(){
    return;
});

gulp.task('build-editor', ['copy:editor'], function() {
    return;
});

gulp.task('build-viewer', ['copy:viewer'], function() {
    return;
});